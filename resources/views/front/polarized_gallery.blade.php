@extends('welcome')

@section('header_scripts')
    <style>
        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }

        .green{
            background-color:#6fb936;
        }

        .thumb{
            margin-bottom: 30px;
        }
                    
        .page-top{
            margin-top:85px;
        }
            
        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }      
            
        .transition {
            -webkit-transform: scale(1.2); 
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }

        .modal-header {    
            border-bottom: none;
        }

        .modal-title {
            color:#000;
        }

        .modal-footer{
            display:none;  
        }

    </style>
@endsection

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home" @if($company->background_image != null) style="background: url({{ $company->background_image }}); background-size: cover;" @endif>	
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Polarizados ADN
					</h1>	
					<p class="text-white link-nav"><a href="{{ route('front.index') }}">Inicio</a></p>
				</div>											
			</div>
		</div>
	</section>
    <!-- End banner Area -->	
            
    <!-- Start service Area -->
	<section class="service-area section-gap" id="service">
        <div class="container page-top">
        <h2>Polarizados ADN</h2><br /><br />
            <div class="row">
                @foreach($images as $image)
                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                        <a href="{{ asset($image->path . '/' . $image->file) }}?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                            <img  src="{{ asset($image->path . '/' . $image->file) }}?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid "  alt="">    
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
	</section>
    <div class="container" style="margin-bottom: 45px;">
        <div class="row">
            <div class="col-md-6 text-center">
                <a class="btn btn-success" @if(strpos($company->whatsapp, 'wa.me')) href="{{ $company->whatsapp }}" @else href="//wa.me/57{{ $company->whatsapp }}" @endif>
                    <i class="fa fa-whatsapp" style="font-size: 50px;"></i>
                </a>
            </div>
            <div class="col-md-6 text-center">
                <a class="btn btn-secondary" href="https://instagram.com/polarizados.adn">
                    <i class="fa fa-instagram" style="font-size: 50px;"></i>
                </a>
            </div>
        </div>
    </div>
	<!-- End service Area -->	
@endsection

@section('footer_scripts')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function(){
            
        		$(this).addClass('transition');
        	}, function(){

        		$(this).removeClass('transition');
        	});
        });
    </script>
@endsection
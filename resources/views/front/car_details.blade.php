@extends('welcome')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home" @if($company->background_image != null) style="background: url({{ $company->background_image }}); background-size: cover;" @endif>	
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						{{ $car->brand->name }}: {{ $car->version }}
					</h1>	
					<p class="text-white link-nav"><a href="{{ route('front.cars') }}">Atrás</a> &nbsp; &nbsp; | &nbsp; &nbsp; <a href="{{ route('front.index') }}">Inicio</a></p>
				</div>											
			</div>
		</div>
	</section>
    <!-- End banner Area -->	
            
    <!-- Start service Area -->
	<section class="service-area section-gap" id="service">
		<div class="container">					
            <a href="{{ route('front.cars') }}" class="btn btn-info">
                Volver
            </a>
            <hr />
			<div class="row">
                <div class="col-lg-8">
					<div class="single-service">
						<div class="thumb">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($car->carImages as $i => $image)
                                    <div class="carousel-item @if($i == 0) active @endif">
                                        <img class="d-block w-100" src="{{ asset('' . $image->path . '/' . $image->file) }}" alt="..." style="max-height: 380px;">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
						</div>
					</div>
					<hr />
					<div class="more-details">
						<div class="detail row">
							@if($car->airbag == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Airbag
								</span>
							@endif
							@if($car->alarm == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Alarma
								</span>
							@endif
							@if($car->stability_control == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Control de estabilidad
								</span>
							@endif
							@if($car->rear_defroster == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Desempañador trasero
								</span>
							@endif
							@if($car->abs_brakes == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Frenos ABS
								</span>
							@endif
							@if($car->air_conditioning == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Aire acondicionado
								</span>
							@endif
							@if($car->electric_mirrors == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Espejos eléctricos
								</span>
							@endif
							@if($car->sliding_roof == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Techo corredizo
								</span>
							@endif
							@if($car->rain_sensor == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Sensor de lluvia
								</span>
							@endif
							@if($car->electroc_glasses == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Vidrios eléctricos
								</span>
							@endif
							@if($car->power_seats == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Asientos eléctricos
								</span>
							@endif
							@if($car->central_locking_of_doors == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Bloqueo central del puertas
								</span>
							@endif
							@if($car->air_conditioner == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Climatizador
								</span>
							@endif
							@if($car->mp3_player == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Reproductor MP3
								</span>
							@endif
							@if($car->bluetooth == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Bluetooth
								</span>
							@endif
							@if($car->dvd == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> DVD
								</span>
							@endif
							@if($car->usb_entry == 'Si')
								<span class="characteristics col-md-4">
									<i class="fa fa-check check-show"></i> Entrada USB
								</span>
							@endif
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="single-service">
						<div class="detail">
							<h4>Detalles</h4>
							<p class="text-justify">
							    <strong>Marca: </strong>{{ $car->brand->name }}<br />
                                <strong>Versión: </strong>{{ $car->version }}<br />
                                <strong>Combustible: </strong>{{ $car->fuel }}<br />
                                <strong>Kilometraje: </strong>{{ number_format($car->mileage, 0, '.', '.') }}<br />
                                <strong>Dirección: </strong>{{ $car->direction }}<br />
                                <strong>Transmisión: </strong>{{ $car->transmission }}<br />
								<strong>Valor: </strong>${{ number_format($car->cost, 0, ',', '.') }}<br />
								<strong>Modelo: </strong>{{ $car->model }}<br />
								<strong>Cilindrada: </strong>{{ $car->cylinder_capacity }}<br />
								<strong>Puertas: </strong>{{ $car->doors }}<br />
								<strong>Placa: </strong>{{ $car->license_plate }}<br />
								<strong>Tracción: </strong>{{ $car->traction }}<br />
								<strong>Carrocería: </strong>{{ $car->body_type }}<br />
								<strong>Ubicación: </strong>{{ $car->vehicle_ubication }}<br />
								<strong>Descripción adicional: </strong>{{ $car->description }}
							</p>
							@if($car->youtube_video != null)
								<a href="{{ $car->youtube_video }}" class="btn btn-danger" target="_blank">
									<i class="fa fa-youtube"></i> Ver en YouTube
								</a>
								<br /><br />
							@endif
							@if($car->user_id !=null)
								<h6>¿Quieres hacer una pregunta?</h6><br />
								<p class="text-justify">
									Puedes escribirnos a WhatsApp.<br /><br />
									<a href="https://api.whatsapp.com/send?phone=+57{{ $car->user->celphone_number }}" class="btn btn-success btn-whatsapp" target=_blank"">
										<i class="fa fa-whatsapp"></i> Enviar mensaje
									</a><br /><br />
									O dejarnos tu mensaje en el siguiente formulario
								</p>
							@endif
							<form action="{{ route('front.askByCar') }}" method="POST">
								{{ @csrf_field() }}
								<input type="hidden" name="car_id" value="{{ $car->id }}">
								<div class="form-group">
									<input type="text" name="name" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<input type="email" name="email" class="form-control" placeholder="E-mail">
								</div>
								<div class="form-group">
									<textarea name="user_message" class="form-control" rows="4">Tu mensaje</textarea>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary">Enviar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</section>
	<!-- End service Area -->	
@endsection

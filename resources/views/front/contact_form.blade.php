@extends('welcome')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home" @if($company->background_image != null) style="background: url({{ $company->background_image }}); background-size: cover;" @endif>	
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Contacto
					</h1>	
					<p class="text-white link-nav"><a href="{{ route('front.index') }}">Inicio</a></p>
				</div>											
			</div>
		</div>
	</section>
    <!-- End banner Area -->	
            
    <!-- Start service Area -->
	<section class="service-area section-gap" id="service">
		<div class="container">					
			<div class="row">
                <div class="col-md-6">
                    <form method="POST" action="{{ route('front.sendMail') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nombre: </label>
                            <input type="text" name="name" class="form-control" required="required" placeholder="Nombres y Apellidos">
                        </div>
                        <div class="form-group">
                            <label>E-mail: </label>
                            <input type="email" name="email" class="form-control" required="required" placeholder="ejemplo@ejemplo">
                        </div>
                        <div class="form-group">
                            <label>Teléfono o Celular: </label>
                            <input type="text" name="phone" class="form-control" required="required" placeholder="Tú # de teléfono o celular">
                        </div>
                        <div class="form-group">
                            <label>Tu mensaje: </label>
                            <textarea name="user_message" class="form-control" required="required"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Enviar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497.0395812868698!2d-74.0544755202251!3d4.714952953712428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f8537e9bc7dd5%3A0x6735dcb75f5dc549!2sBambourie-%20Productos%20De%20Aseo!5e0!3m2!1ses!2sco!4v1593483397064!5m2!1ses!2sco" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
			</div>
		</div>	
	</section>
	<!-- End service Area -->	
@endsection

@extends('welcome')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home" @if($company->background_image != null) style="background: url({{ $company->background_image }}); background-size: cover;" @endif>	
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Autos
					</h1>	
					<p class="text-white link-nav"><a href="{{ route('front.index') }}">Inicio</a></p>
				</div>											
			</div>
		</div>
	</section>
    <!-- End banner Area -->	
            
    <!-- Start service Area -->
	<section class="service-area section-gap" id="service">
		<div class="container">	
            <center>
            <div class="col-md-12 text-center">
                <form class="form-inline" action="{{ route('front.cars') }}" method="GET" role="search">
                    <div class="input-group">
                        <select name="brand_id" class="form-control" aria-describedby="search">
                            <option value="0">Seleccionar marca</option>
                            @foreach($brands as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group">
                        <select name="range_cost" class="form-control" aria-describedby="search">
                            <option value="0">Seleccionar rango de costo</option>
                            <option value="1-10">$1'000.000 a $10'000.000</option>
                            <option value="10-20">$10'000.000 a $20'000.000</option>
                            <option value="20-30">$20'000.000 a $30'000.000</option>
                            <option value="30-40">$30'000.000 a $40'000.000</option>
                            <option value="40-50">$40'000.000 a $50'000.000</option>
                            <option value="50-60">$50'000.000 a $60'000.000</option>
                            <option value="60-70">$60'000.000 a $70'000.000</option>
                            <option value="70-80">$70'000.000 a $80'000.000</option>
                            <option value="80-90">$80'000.000 a $90'000.000</option>
                            <option value="90-100">$90'000.000 a $100'000.000</option>
                            <option value="100-max">$100'000.000 en adelante</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <select name="range_mileage" class="form-control" aria-describedby="search">
                            <option value="0">Seleccionar rango de kilometraje</option>
                            <option value="1-10">1.000 a 10.000</option>
                            <option value="10-20">10.000 a 20.000</option>
                            <option value="20-30">20.000 a 30.000</option>
                            <option value="30-40">30.000 a 40.000</option>
                            <option value="40-50">40.000 a 50.000</option>
                            <option value="50-60">50.000 a 60.000</option>
                            <option value="60-70">60.000 a 70.000</option>
                            <option value="70-80">70.000 a 80.000</option>
                            <option value="80-90">80.000 a 90.000</option>
                            <option value="90-100">90.000 a 100.000</option>
                            <option value="100-max">100.000 en adelante</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </div>	
        </center>			
			<div class="row">
                @foreach($cars as $car)
                    <div class="col-lg-4">
                        <div class="single-service">
                            <img src="{{ asset('/' . $car->mainImage->path . '/' . $car->mainImage->file) }}" class="d-block w-100" style="max-height: 150px">
                        </div>
                        <div class="detail">
                            <h4></h4>
                            <p>
                                <strong>Marca: </strong>{{ $car->brand->name }}<br />
                                <strong>Modelo: </strong>{{ $car->version }}<br />
                                <strong>Combustible: </strong>{{ $car->fuel }}<br />
                                <strong>Precio: </strong>$ {{ number_format($car->cost, 0, ',', '.') }}<br />
                                <strong>Kilometraje: </strong>{{ $car->mileage }}<br />
                                <p class="text-center">
                                    <a href="{{ route('front.carDetails', $car->id) }}" class="btn btn-primary">
                                        Ver más
                                    </a>
                                </p>
                            </p>
                        </div>
                    </div>		
                @endforeach						
			</div>
		</div>	
	</section>
	<!-- End service Area -->	

    <div class="container" style="margin-bottom: 45px;">
        <div class="row">
            <div class="col-md-6 text-center">
                <a class="btn btn-success" @if(strpos($company->whatsapp, 'wa.me')) href="{{ $company->whatsapp }}" @else href="//wa.me/57{{ $company->whatsapp }}" @endif>
                    <i class="fa fa-whatsapp" style="font-size: 50px;"></i>
                </a>
            </div>
            <div class="col-md-6 text-center">
                <a class="btn btn-secondary" href="https://instagram.com/automovilesdelnorte">
                    <i class="fa fa-instagram" style="font-size: 50px;"></i>
                </a>
            </div>
        </div>
    </div>
@endsection

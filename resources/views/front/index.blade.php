@extends('welcome')

@section('header_scripts')
    <style>
        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }

        .green{
            background-color:#6fb936;
        }

        .thumb{
            margin-bottom: 30px;
        }
                    
        .page-top{
            margin-top:85px;
        }
            
        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }      
            
        .transition {
            -webkit-transform: scale(1.2); 
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }

        .modal-header {    
            border-bottom: none;
        }

        .modal-title {
            color:#000;
        }

        .modal-footer{
            display:none;  
        }

		.social-icons {
		    list-style: none;
		    padding: 0;
		    margin: 0;
		}
		
		.social-icons > li {
		    display: inline-block;
		}
		
		.social-icons > li > a {
		    background: #f5f5f5;
		    display: block;
		    margin: 0 8px 8px 0;
		    text-align: center;
		    line-height: 32px;
		    font-size: 14px;
		    height: 32px;
		    width: 32px;
		    color: #777777;
		}
		
		.social-icons > li > a:hover,
		.social-icons > li > a:focus {
		    background: #27CBC0;
		    color: #ffffff;
		}
		
		/* --- [ Social Icons Size ] --- */
		
		.social-icons-lg > li > a {
		    line-height: 48px;
		    font-size: 16px;
		    height: 100px;
		    width: 100px;
		}

		.social-icons-md > li > a {
		    line-height: 40px;
		    height: 40px;
		    width: 40px;
		}

		/* --- [ Social Icons Mode ] --- */

		.social-icons-round > li > a {
		    border-radius: 2px;
		}

		.social-icons-circle > li > a {
		    border-radius: 100%;
		}

		/* --- [ Social Icons Colored ] --- */

		.social-icons-colored > li > a {
		    color: #ffffff;
		}

		.social-icons-colored > li > a:hover,
		.social-icons-colored > li > a:focus {
		    opacity: 0.85;
		}

		/* --- [ Social Icons Simple ] --- */

		.social-icons-simple > li > a {
		    display: inline;
		    background: none;
		    margin: 0 10px 10px 0;
		}

		.social-icons-simple > li > a:hover,
		.social-icons-simple > li > a:focus {
		    background: none;
		    color: inherit;
		}

		/* --- [ Backgrounds ] --- */

		.bg-white {
		    background-color: #ffffff !important;
		}

		.bg-white-dark {
		    background-color: #f8f8f8 !important;
		}

		.bg-white-darken {
		    background-color: #f5f5f5 !important;
		}

		.bg-gray {
		    background-color: #777777 !important;
		}

		.bg-gray-light {
		    background-color: #999999 !important;
		}

		.bg-gray-lighter {
		    background-color: #eeeeee !important;
		}

		.bg-black {
		    background-color: #000000 !important;
		}

		.bg-black-light {
		    background-color: #222222 !important;
		}

		.bg-black-lighter {
		    background-color: #333333 !important;
		}

		.bg-brand {
		    background-color: #27CBC0 !important;
		}

		.bg-brand-hvr {
		    background-color: #1fa098 !important;
		}

		.bg-facebook {
		    background-color: #3b5998 !important;
		}

		.bg-twitter {
		    background-color: #00aced !important;
		}

		.bg-google {
		    background-color: #dd4b39 !important;
		}

		.bg-linkedin {
		    background-color: #007bb6 !important;
		}

		.bg-youtube {
		    background-color: #bb0000 !important;
		}

		.bg-instagram {
		    background-color: #517fa4 !important;
		}

		.bg-pinterest {
		    background-color: #cb2027 !important;
		}

		.bg-flickr {
		    background-color: #ff0084 !important;
		}

		.bg-tumblr {
		    background-color: #32506d !important;
		}

		.bg-forusquare {
		    background-color: #0072b1 !important;
		}

		.bg-dribbble {
		    background-color: #ea4c89 !important;
		}

		.bg-vine {
		    background-color: #00bf8f !important;
		}

		.bg-skype {
		    background-color: #17a3eb !important;
		}

		.bg-wordpress {
		    background-color: #2592c3 !important;
		}

		.bg-behance {
		    background-color: #1879fd !important;
		}

		.bg-soundcloud {
		    background-color: #ff7e30 !important;
		}

		.bg-stumbleupon {
		    background-color: #ff5c30 !important;
		}

		.bg-deviantart {
		    background-color: #6a8a7b !important;
		}

		.bg-yahoo {
		    background-color: #ab47ac !important;
		}

		.bg-digg {
		    background-color: #75788d !important;
		}

		.bg-github {
		    background-color: #3f91cb !important;
		}

		.bg-lastfm {
		    background-color: #f34320 !important;
		}

		.bg-vk {
		    background-color: #2b587a !important;
		}

		/* --- [ Alfa backgrounds ] --- */

		.bg-black-alfa-5:before,
		.bg-black-alfa-10:before,
		.bg-black-alfa-15:before,
		.bg-black-alfa-20:before,
		.bg-black-alfa-25:before,
		.bg-black-alfa-30:before,
		.bg-black-alfa-35:before,
		.bg-black-alfa-40:before,
		.bg-black-alfa-45:before,
		.bg-black-alfa-50:before,
		.bg-black-alfa-55:before,
		.bg-black-alfa-60:before,
		.bg-black-alfa-65:before,
		.bg-black-alfa-70:before,
		.bg-black-alfa-75:before,
		.bg-black-alfa-80:before,
		.bg-black-alfa-85:before,
		.bg-black-alfa-90:before,
		.bg-black-alfa-95:before {
		    position: absolute;
		    height: 100%;
		    width: 100%;
		    z-index: 0;
		    content: "";
		    left: 0;
		    top: 0;
		}

		.bg-black-alfa-5:before,
		.bg-black-alfa-10:before,
		.bg-black-alfa-15:before,
		.bg-black-alfa-20:before,
		.bg-black-alfa-25:before,
		.bg-black-alfa-30:before,
		.bg-black-alfa-35:before,
		.bg-black-alfa-40:before,
		.bg-black-alfa-45:before,
		.bg-black-alfa-50:before,
		.bg-black-alfa-55:before,
		.bg-black-alfa-60:before,
		.bg-black-alfa-65:before,
		.bg-black-alfa-70:before,
		.bg-black-alfa-75:before,
		.bg-black-alfa-80:before,
		.bg-black-alfa-85:before,
		.bg-black-alfa-90:before,
		.bg-black-alfa-95:before {
		    background: rgba(0, 0, 0, 0.05);
		}

		.bg-black-alfa-10:before {
		    background: rgba(0, 0, 0, 0.1);
		}

		.bg-black-alfa-15:before {
		    background: rgba(0, 0, 0, 0.15);
		}

		.bg-black-alfa-20:before {
		    background: rgba(0, 0, 0, 0.2);
		}

		.bg-black-alfa-25:before {
		    background: rgba(0, 0, 0, 0.25);
		}

		.bg-black-alfa-30:before {
		    background: rgba(0, 0, 0, 0.3);
		}

		.bg-black-alfa-35:before {
		    background: rgba(0, 0, 0, 0.35);
		}

		.bg-black-alfa-40:before {
		    background: rgba(0, 0, 0, 0.4);
		}

		.bg-black-alfa-45:before {
		    background: rgba(0, 0, 0, 0.45);
		}

		.bg-black-alfa-50:before {
		    background: rgba(0, 0, 0, 0.5);
		}

		.bg-black-alfa-55:before {
		    background: rgba(0, 0, 0, 0.55);
		}

		.bg-black-alfa-60:before {
		    background: rgba(0, 0, 0, 0.6);
		}

		.bg-black-alfa-65:before {
		    background: rgba(0, 0, 0, 0.65);
		}

		.bg-black-alfa-70:before {
		    background: rgba(0, 0, 0, 0.7);
		}

		.bg-black-alfa-75:before {
		    background: rgba(0, 0, 0, 0.75);
		}

		.bg-black-alfa-80:before {
		    background: rgba(0, 0, 0, 0.8);
		}

		.bg-black-alfa-85:before {
		    background: rgba(0, 0, 0, 0.85);
		}

		.bg-black-alfa-90:before {
		    background: rgba(0, 0, 0, 0.9);
		}

		.bg-black-alfa-95:before {
		    background: rgba(0, 0, 0, 0.95);
		}

		.bg-white-alfa-5:before,
		.bg-white-alfa-10:before,
		.bg-white-alfa-15:before,
		.bg-white-alfa-20:before,
		.bg-white-alfa-25:before,
		.bg-white-alfa-30:before,
		.bg-white-alfa-35:before,
		.bg-white-alfa-40:before,
		.bg-white-alfa-45:before,
		.bg-white-alfa-50:before,
		.bg-white-alfa-55:before,
		.bg-white-alfa-60:before,
		.bg-white-alfa-65:before,
		.bg-white-alfa-70:before,
		.bg-white-alfa-75:before,
		.bg-white-alfa-80:before,
		.bg-white-alfa-85:before,
		.bg-white-alfa-90:before,
		.bg-white-alfa-95:before {
		    position: absolute;
		    height: 100%;
		    width: 100%;
		    z-index: 0;
		    content: "";
		    left: 0;
		    top: 0;
		}

		.bg-white-alfa-5:before,
		.bg-white-alfa-10:before,
		.bg-white-alfa-15:before,
		.bg-white-alfa-20:before,
		.bg-white-alfa-25:before,
		.bg-white-alfa-30:before,
		.bg-white-alfa-35:before,
		.bg-white-alfa-40:before,
		.bg-white-alfa-45:before,
		.bg-white-alfa-50:before,
		.bg-white-alfa-55:before,
		.bg-white-alfa-60:before,
		.bg-white-alfa-65:before,
		.bg-white-alfa-70:before,
		.bg-white-alfa-75:before,
		.bg-white-alfa-80:before,
		.bg-white-alfa-85:before,
		.bg-white-alfa-90:before,
		.bg-white-alfa-95:before {
		    background: rgba(255, 255, 255, 0.05);
		}

		.bg-white-alfa-10:before {
		    background: rgba(255, 255, 255, 0.1);
		}

		.bg-white-alfa-15:before {
		    background: rgba(255, 255, 255, 0.15);
		}

		.bg-white-alfa-20:before {
		    background: rgba(255, 255, 255, 0.2);
		}

		.bg-white-alfa-25:before {
		    background: rgba(255, 255, 255, 0.25);
		}

		.bg-white-alfa-30:before {
		    background: rgba(255, 255, 255, 0.3);
		}

		.bg-white-alfa-35:before {
		    background: rgba(255, 255, 255, 0.35);
		}

		.bg-white-alfa-40:before {
		    background: rgba(255, 255, 255, 0.4);
		}

		.bg-white-alfa-45:before {
		    background: rgba(255, 255, 255, 0.45);
		}

		.bg-white-alfa-50:before {
		    background: rgba(255, 255, 255, 0.5);
		}

		.bg-white-alfa-55:before {
		    background: rgba(255, 255, 255, 0.55);
		}

		.bg-white-alfa-60:before {
		    background: rgba(255, 255, 255, 0.6);
		}

		.bg-white-alfa-65:before {
		    background: rgba(255, 255, 255, 0.65);
		}

		.bg-white-alfa-70:before {
		    background: rgba(255, 255, 255, 0.7);
		}

		.bg-white-alfa-75:before {
		    background: rgba(255, 255, 255, 0.75);
		}

		.bg-white-alfa-80:before {
		    background: rgba(255, 255, 255, 0.8);
		}

		.bg-white-alfa-85:before {
		    background: rgba(255, 255, 255, 0.85);
		}

		.bg-white-alfa-90:before {
		    background: rgba(255, 255, 255, 0.9);
		}

		.bg-white-alfa-95:before {
		    background: rgba(255, 255, 255, 0.95);
		}

		.bg-brand-alfa-5:before,
		.bg-brand-alfa-10:before,
		.bg-brand-alfa-15:before,
		.bg-brand-alfa-20:before,
		.bg-brand-alfa-25:before,
		.bg-brand-alfa-30:before,
		.bg-brand-alfa-35:before,
		.bg-brand-alfa-40:before,
		.bg-brand-alfa-45:before,
		.bg-brand-alfa-50:before,
		.bg-brand-alfa-55:before,
		.bg-brand-alfa-60:before,
		.bg-brand-alfa-65:before,
		.bg-brand-alfa-70:before,
		.bg-brand-alfa-75:before,
		.bg-brand-alfa-80:before,
		.bg-brand-alfa-85:before,
		.bg-brand-alfa-90:before,
		.bg-brand-alfa-95:before {
		    position: absolute;
		    height: 100%;
		    width: 100%;
		    z-index: 0;
		    content: "";
		    left: 0;
		    top: 0;
		}

		.bg-brand-alfa-5:before,
		.bg-brand-alfa-10:before,
		.bg-brand-alfa-15:before,
		.bg-brand-alfa-20:before,
		.bg-brand-alfa-25:before,
		.bg-brand-alfa-30:before,
		.bg-brand-alfa-35:before,
		.bg-brand-alfa-40:before,
		.bg-brand-alfa-45:before,
		.bg-brand-alfa-50:before,
		.bg-brand-alfa-55:before,
		.bg-brand-alfa-60:before,
		.bg-brand-alfa-65:before,
		.bg-brand-alfa-70:before,
		.bg-brand-alfa-75:before,
		.bg-brand-alfa-80:before,
		.bg-brand-alfa-85:before,
		.bg-brand-alfa-90:before,
		.bg-brand-alfa-95:before {
		    background: rgba(39, 203, 192, 0.05);
		}

		.bg-brand-alfa-10:before {
		    background: rgba(39, 203, 192, 0.1);
		}

		.bg-brand-alfa-15:before {
		    background: rgba(39, 203, 192, 0.15);
		}

		.bg-brand-alfa-20:before {
		    background: rgba(39, 203, 192, 0.2);
		}

		.bg-brand-alfa-25:before {
		    background: rgba(39, 203, 192, 0.25);
		}

		.bg-brand-alfa-30:before {
		    background: rgba(39, 203, 192, 0.3);
		}

		.bg-brand-alfa-35:before {
		    background: rgba(39, 203, 192, 0.35);
		}

		.bg-brand-alfa-40:before {
		    background: rgba(39, 203, 192, 0.4);
		}

		.bg-brand-alfa-45:before {
		    background: rgba(39, 203, 192, 0.45);
		}

		.bg-brand-alfa-50:before {
		    background: rgba(39, 203, 192, 0.5);
		}

		.bg-brand-alfa-55:before {
		    background: rgba(39, 203, 192, 0.55);
		}

		.bg-brand-alfa-60:before {
		    background: rgba(39, 203, 192, 0.6);
		}

		.bg-brand-alfa-65:before {
		    background: rgba(39, 203, 192, 0.65);
		}

		.bg-brand-alfa-70:before {
		    background: rgba(39, 203, 192, 0.7);
		}

		.bg-brand-alfa-75:before {
		    background: rgba(39, 203, 192, 0.75);
		}

		.bg-brand-alfa-80:before {
		    background: rgba(39, 203, 192, 0.8);
		}

		.bg-brand-alfa-85:before {
		    background: rgba(39, 203, 192, 0.85);
		}

		.bg-brand-alfa-90:before {
		    background: rgba(39, 203, 192, 0.9);
		}

		.bg-brand-alfa-95:before {
		    background: rgba(39, 203, 192, 0.95);
		}

		.bg-white-dark-alfa-5:before,
		.bg-white-dark-alfa-10:before,
		.bg-white-dark-alfa-15:before,
		.bg-white-dark-alfa-20:before,
		.bg-white-dark-alfa-25:before,
		.bg-white-dark-alfa-30:before,
		.bg-white-dark-alfa-35:before,
		.bg-white-dark-alfa-40:before,
		.bg-white-dark-alfa-45:before,
		.bg-white-dark-alfa-50:before,
		.bg-white-dark-alfa-55:before,
		.bg-white-dark-alfa-60:before,
		.bg-white-dark-alfa-65:before,
		.bg-white-dark-alfa-70:before,
		.bg-white-dark-alfa-75:before,
		.bg-white-dark-alfa-80:before,
		.bg-white-dark-alfa-85:before,
		.bg-white-dark-alfa-90:before,
		.bg-white-dark-alfa-95:before {
		    position: absolute;
		    height: 100%;
		    width: 100%;
		    z-index: 0;
		    content: "";
		    left: 0;
		    top: 0;
		}

		.bg-white-dark-alfa-5:before,
		.bg-white-dark-alfa-10:before,
		.bg-white-dark-alfa-15:before,
		.bg-white-dark-alfa-20:before,
		.bg-white-dark-alfa-25:before,
		.bg-white-dark-alfa-30:before,
		.bg-white-dark-alfa-35:before,
		.bg-white-dark-alfa-40:before,
		.bg-white-dark-alfa-45:before,
		.bg-white-dark-alfa-50:before,
		.bg-white-dark-alfa-55:before,
		.bg-white-dark-alfa-60:before,
		.bg-white-dark-alfa-65:before,
		.bg-white-dark-alfa-70:before,
		.bg-white-dark-alfa-75:before,
		.bg-white-dark-alfa-80:before,
		.bg-white-dark-alfa-85:before,
		.bg-white-dark-alfa-90:before,
		.bg-white-dark-alfa-95:before {
		    background: rgba(248, 248, 248, 0.05);
		}

		.bg-white-dark-alfa-10:before {
		    background: rgba(248, 248, 248, 0.1);
		}

		.bg-white-dark-alfa-15:before {
		    background: rgba(248, 248, 248, 0.15);
		}

		.bg-white-dark-alfa-20:before {
		    background: rgba(248, 248, 248, 0.2);
		}

		.bg-white-dark-alfa-25:before {
		    background: rgba(248, 248, 248, 0.25);
		}

		.bg-white-dark-alfa-30:before {
		    background: rgba(248, 248, 248, 0.3);
		}

		.bg-white-dark-alfa-35:before {
		    background: rgba(248, 248, 248, 0.35);
		}

		.bg-white-dark-alfa-40:before {
		    background: rgba(248, 248, 248, 0.4);
		}

		.bg-white-dark-alfa-45:before {
		    background: rgba(248, 248, 248, 0.45);
		}

		.bg-white-dark-alfa-50:before {
		    background: rgba(248, 248, 248, 0.5);
		}

		.bg-white-dark-alfa-55:before {
		    background: rgba(248, 248, 248, 0.55);
		}

		.bg-white-dark-alfa-60:before {
		    background: rgba(248, 248, 248, 0.6);
		}

		.bg-white-dark-alfa-65:before {
		    background: rgba(248, 248, 248, 0.65);
		}

		.bg-white-dark-alfa-70:before {
		    background: rgba(248, 248, 248, 0.7);
		}

		.bg-white-dark-alfa-75:before {
		    background: rgba(248, 248, 248, 0.75);
		}

		.bg-white-dark-alfa-80:before {
		    background: rgba(248, 248, 248, 0.8);
		}

		.bg-white-dark-alfa-85:before {
		    background: rgba(248, 248, 248, 0.85);
		}

		.bg-white-dark-alfa-90:before {
		    background: rgba(248, 248, 248, 0.9);
		}

		.bg-white-dark-alfa-95:before {
		    background: rgba(248, 248, 248, 0.95);
		}
    </style>
@endsection

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home" @if($company->background_image != null) style="background: url({{ $company->background_image }}); background-size: cover;" @endif>
		<div class="overlay overlay-bg"></div>	
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-start">
				<div class="banner-content col-lg-9">
					<h6 class="text-white"></h6>
					<h1 class="text-white">
						{{ $company->name }}
					</h1>
					<p class="pt-20 pb-20 text-white">
						{{ $company->description }}
					</p>
					<a href="#service" class="primary-btn text-uppercase">Conócenos</a>
				</div>											
			</div>
		</div>					
	</section>
	<!-- End banner Area -->
	
	<!-- Start service Area -->
	<section class="service-area section-gap" id="service" style="background: #6c757d; padding-bottom: 0px !important;">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10" style="color: #ffffff">Objetivo y enfoque</h1>
						<p class="text-center" style="color: #ffffff">{{ $company->objectives }}</p>
					</div>
				</div>
			</div>						
		</div>	
	</section>
	<!-- End service Area -->	

	<section class="service-area section-gap" id="service" style="padding-bottom: 0px !important; padding-top: 0px !important;">
		<div class="container-fluid">				
			<div class="row">
				<div class="col-lg-6">
					<div class="thumb">
						@if($company->carshop_image != null)
							<img class="img-fluid" src="{{ asset($company->carshop_image) }}" alt="" style="width: 100%; height: 100%;">
						@else
							<img class="img-fluid" src="{{ asset('img/front/s1.jpg') }}" alt="" style="width: 100%; height: 100%;">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="detail text-center">
						@if($company->carshop_info != null)
							<p class="text-center" style="vertical-align: middle; margin-top: 25%">
								<h1 class="mb-10">Autos usados</h1>
							    {{ $company->carshop_info }}
							</p>
						@else
							<p class="text-justify">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus dapibus justo, nec gravida lorem fringilla sit amet. Proin finibus sagittis arcu eget tristique. Nullam a mattis tellus. Nam nec lorem vitae purus consequat aliquam. Pellentesque at bibendum dui. Pellentesque eu risus at dui ornare porta sit amet ut turpis. Nunc ut metus sapien. Cras ac varius sapien. 
							</p>
						@endif
						<a class="btn btn-secondary btn-lg" href="{{ route('front.cars') }}">Ver Autos</a>
					</div>
				</div>											
			</div>
		</div>	
	</section>

	<section class="service-area section-gap" id="feature" style="padding-top: 0px !important; padding-bottom: 0px !important; background: #eee; border-top: 15px solid #0E0108; border-bottom: 15px solid #0E0108">
		<div class="container-fluid">					
			<div class="row">
				<div class="col-lg-6">
					<div class="detail text-center">
					@if($company->polarized_info != null)
						<p class="text-center" style="vertical-align: middle; margin-top: 25%">
							<h1 class="mb-10">Polarizados</h1>
						    {{ $company->polarized_info }}
						</p>
					@else
						<p class="text-justify">
						    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus dapibus justo, nec gravida lorem fringilla sit amet. Proin finibus sagittis arcu eget tristique. Nullam a mattis tellus. Nam nec lorem vitae purus consequat aliquam. Pellentesque at bibendum dui. Pellentesque eu risus at dui ornare porta sit amet ut turpis. Nunc ut metus sapien. Cras ac varius sapien. 
						</p>
					@endif
					<a class="btn btn-secondary btn-lg" href="{{ route('front.polarizedGallery') }}">Ver Galería de Polarizados</a>
					</div>
				</div>		
				<div class="col-lg-6">
					<div class="thumb">
						@if($company->polarized_image != null)
							<img class="img-fluid" src="{{ asset($company->polarized_image) }}" alt="" style="width: 100%; height: 100%;">
						@else
							<img class="img-fluid" src="{{ asset('img/front/s2.jpg') }}" alt="" style="width: 100%; height: 100%;">
						@endif
					</div>
				</div>									
			</div>
		</div>	
	</section>
	
	<section class="service-area section-gap" id="service" style="padding-top: 0px !important; padding-bottom: 0px !important; border-bottom: 15px solid #0E0108">
		<div class="container-fluid">				
			<div class="row">
				<div class="col-lg-6">
					<div class="thumb">
						@if($company->upholstery_image != null)
							<img class="img-fluid" src="{{ asset($company->upholstery_image) }}" alt="" style="width: 100%; height: 100%;">
						@else
							<img class="img-fluid" src="{{ asset('img/front/s3.jpeg') }}" alt="" style="width: 100%; height: 100%;">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="detail text-center">
						@if($company->upholstery_info != null)
							<p class="text-center" style="vertical-align: middle; margin-top: 25%">
								<h1 class="mb-10">Tapicería</h1>
							    {{ $company->upholstery_info }}
							</p>
						@else
							<p class="text-justify">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus dapibus justo, nec gravida lorem fringilla sit amet. Proin finibus sagittis arcu eget tristique. Nullam a mattis tellus. Nam nec lorem vitae purus consequat aliquam. Pellentesque at bibendum dui. Pellentesque eu risus at dui ornare porta sit amet ut turpis. Nunc ut metus sapien. Cras ac varius sapien. 
							</p>
						@endif
						<a class="btn btn-secondary btn-lg" href="{{ route('front.seatingGallery') }}">Ver Galería de Tapicería</a>
					</div>
				</div>											
			</div>
		</div>	
	</section>

	<section class="service-area section-gap" id="contact" style="background: #eee; display: none;">
		<div class="container">		
			<div class="text-center">
				<h1 class="mb-10">Contacto</h1>
			</div>
			<br /><br />
			<div class="row">
                <div class="col-lg-6">
                    <form method="POST" action="{{ route('front.sendMail') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nombre: </label>
                            <input type="text" name="name" class="form-control" required="required" placeholder="Nombres y Apellidos">
                        </div>
                        <div class="form-group">
                            <label>E-mail: </label>
                            <input type="email" name="email" class="form-control" required="required" placeholder="ejemplo@ejemplo">
                        </div>
                        <div class="form-group">
                            <label>Teléfono o Celular: </label>
                            <input type="text" name="phone" class="form-control" required="required" placeholder="Tú # de teléfono o celular">
                        </div>
                        <div class="form-group">
                            <label>Tu mensaje: </label>
                            <textarea name="user_message" class="form-control" required="required"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-secondary">
                                Enviar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15905.138812233638!2d-74.0552716!3d4.720531!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5aeb9d6c5b1612cc!2sAutom%C3%B3viles%20Del%20Norte%20SAS!5e0!3m2!1ses!2sco!4v1638407295064!5m2!1ses!2sco" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
			</div>
		</div>	
	</section>

	<section class="service-area section-gap" id="contact" style="background: #eee;">
		<div class="container">		
			<div class="text-center">
				<h1 class="mb-10">Contacto</h1>
			</div>
			<br /><br />
			<div class="row">
                <div class="col-lg-6">
					<ul class="social-icons social-icons-lg social-icons-colored social-icons-round m-t-50">
						<li><a target="_blank" href="{{ $company->facebook }}" class="bg-facebook"><i style="margin-top: 30%; font-size: 40px;" class="fa fa-facebook"></i></a></li>
						<li><a target="_blank" href="mailto:automovilesdelnorte@gmail.com" class="bg-vk"><i style="margin-top: 30%; font-size: 40px;" class="fa fa-envelope"></i></a></li>
						<li><a target="_blank" href="{{ $company->instagram }}" class="bg-instagram"><i style="margin-top: 30%; font-size: 40px;" class="fa fa-instagram"></i></a></li>
						<li><a target="_blank" @if(strpos($company->whatsapp, 'wa.me')) href="{{ $company->whatsapp }}" @else href="//wa.me/57{{ $company->whatsapp }}" @endif class="bg-vine"><i style="margin-top: 30%; font-size: 40px;" class="fa fa-whatsapp"></i></a></li>
					</ul>
					<br />
					<h2><i class="fa fa-phone-square" aria-hidden="true"></i> Llámanos al {{ $company->phone_number }}</h2>
                </div>
                <div class="col-lg-6">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15905.138812233638!2d-74.0552716!3d4.720531!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5aeb9d6c5b1612cc!2sAutom%C3%B3viles%20Del%20Norte%20SAS!5e0!3m2!1ses!2sco!4v1638407295064!5m2!1ses!2sco" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
			</div>
		</div>	
	</section>

	@if($company->mission != null && $company->vission != null)
		<!-- Start feature Area -->
		<section class="feature-area section-gap" id="feature">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="single-feature">
							<h4><span class="lnr lnr-flag"></span>Misión</h4>
							<p>
								{{ $company->mission }}
							</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="single-feature">
							<h4><span class="lnr lnr-star"></span>Visión</h4>
							<p>
								{{ $company->vission }}
							</p>								
						</div>
					</div>						
				</div>
			</div>	
		</section>
		<!-- End feature Area -->	
	@endif								

	<!-- Start galery Area -->
	<section class="galery-area section-gap" id="gallery" style="display: none;">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Instapost</h1>
					</div>
				</div>
			</div>					
			<div class="row">
				<div class="instagram_feed" id="adn-instagram"></div>
				<div class="instagram_feed" id="tapicerias-instagram"></div>
				<div class="instagram_feed" id="polarizados-instagram"></div>
			</div>
		</div>	
	</section>
    <!-- End galery Area -->
@endsection

@section('footer_scripts')
	<script src="{{ asset('js/front/instagram/jquery.instagramFeed.min.js') }}"></script>
	<script>
		$(function($){
			$(window).on('load', function(){
				$.instagramFeed({
					'username': 'Automovilesdelnorte',
					'container': "#adn-instagram",
					'display_profile': true,
					'display_biography': true,
					'display_gallery': true,
					'callback': null,
					'styling': true,
					'items': 8,
					'items_per_row': 8,
					'margin': 1 
				});

				$.instagramFeed({
					'username': 'tapicerias.adn',
					'container': "#tapicerias-instagram",
					'display_profile': true,
					'display_biography': true,
					'display_gallery': true,
					'callback': null,
					'styling': true,
					'items': 8,
					'items_per_row': 8,
					'margin': 1 
				});

				$.instagramFeed({
					'username': 'polarizados.adn',
					'container': "#polarizados-instagram",
					'display_profile': true,
					'display_biography': true,
					'display_gallery': true,
					'callback': null,
					'styling': true,
					'items': 8,
					'items_per_row': 8,
					'margin': 1 
				});
			});
    	});
	</script>
@endsection

@section('footer_scripts')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function(){
            
        		$(this).addClass('transition');
        	}, function(){

        		$(this).removeClass('transition');
        	});
        });
    </script>
@endsection
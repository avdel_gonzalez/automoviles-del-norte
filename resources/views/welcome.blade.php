<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="{{ asset('img/front/fav.png') }}">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Automóviles del norte</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="{{ asset('css/front/linearicons.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/font-awesome.min.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/bootstrap.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/magnific-popup.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/nice-select.css') }}">					
			<link rel="stylesheet" href="{{ asset('css/front/animate.min.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/owl.carousel.css') }}">
			<link rel="stylesheet" href="{{ asset('css/front/main.css') }}">
			@yield('header_scripts')
		</head>
		<body>

			  <header id="header" id="home">
			  	<div class="container header-top">
			  		<div class="row">
				  		<div class="col-6 top-head-left">				  			
				  			<ul>
							  	<li><a target="_blank" href="https://www.facebook.com/AutomovilesDelNorte/"><i class="fa fa-facebook"></i></a></li>
								<li><a target="_blank" href="https://www.instagram.com/automovilesdelnorte"><i class="fa fa-instagram"></i></a></li>
								<li><a target="_blank" href="https://www.youtube.com/channel/UC-wCyzyIDe8zq9pmmPO4AwQ"><i class="fa fa-youtube"></i></a></li>
								<li><a target="_blank" href="//wa.me/573143337798"><i class="fa fa-whatsapp"></i></a></li>
				  			</ul>
				  		</div>		  			
			  		</div>
			  	</div>
			  	<hr>
			    <div class="container">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				      
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li class="menu-active"><a href="{{ route('front.index') }}">Inicio</a></li>
						  <li><a href="{{ route('front.cars') }}">Autos</a></li>
				          <li><a href="{{ route('front.polarizedGallery') }}">Polarizados</a></li>			          
						  <li><a href="{{ route('front.seatingGallery') }}">Tapicería</a></li>
                          <!-- <li><a href="{{ route('front.contactForm') }}">Contacto</a></li> -->
				        </ul>
				      </nav><!-- #nav-menu-container -->		    		
			    	</div>
			    </div>
			  </header><!-- #header -->	
				@yield('content')
			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Automóviles del norte</h6>
								<p class="text-justify">
                                    {{ $company->footer_text }}
								</p>							
							</div>
						</div>
						<div class="col-lg-5  col-md-6 col-sm-6">
							
						</div>						
						<div class="col-lg-2 col-md-6 col-sm-6 social-widget">
							<div class="single-footer-widget text-right">
								<h6>Síguenos</h6>
								<p>Redes sociales</p>
								<div class="footer-social d-flex align-items-center">
									<a target="_blank" href="https://www.facebook.com/AutomovilesDelNorte/"><i class="fa fa-facebook"></i></a>
									<a target="_blank" href="https://www.instagram.com/automovilesdelnorte"><i class="fa fa-instagram"></i></a>
									<a target="_blank" href="https://www.youtube.com/channel/UC-wCyzyIDe8zq9pmmPO4AwQ"><i class="fa fa-youtube"></i></a>
									<a target="_blank" href="//wa.me/573143337798"><i class="fa fa-whatsapp"></i></a>
								</div>
							</div>
						</div>							
					</div>
				</div>
			</footer>	
			<!-- End footer Area -->		

			<script src="{{ asset('js/front/vendor/jquery-2.2.4.min.js') }}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="{{ asset('js/front/vendor/bootstrap.min.js') }}"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="{{ asset('js/front/easing.min.js') }}"></script>			
			<script src="{{ asset('js/front/hoverIntent.js') }}"></script>
			<script src="{{ asset('js/front/superfish.min.js') }}"></script>	
			<script src="{{ asset('js/front/jquery.ajaxchimp.min.js') }}"></script>
			<script src="{{ asset('js/front/jquery.magnific-popup.min.js') }}"></script>	
			<script src="{{ asset('js/front/owl.carousel.min.js') }}"></script>			
			<script src="{{ asset('js/front/jquery.sticky.js') }}"></script>
			<script src="{{ asset('js/front/jquery.nice-select.min.js') }}"></script>	
			<script src="{{ asset('js/front/waypoints.min.js') }}"></script>
			<script src="{{ asset('js/front/jquery.counterup.min.js') }}"></script>					
			<script src="{{ asset('js/front/parallax.min.js') }}"></script>		
			<script src="{{ asset('js/front/mail-script.js') }}"></script>	
			<script src="{{ asset('js/front/main.js') }}"></script>	
			@yield('footer_scripts')
		</body>
	</html>

@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Nueva marca</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('brands.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label class="label-form">Nombre</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" required="required" placeholder="Nombre de la marca">
                </div>
                <label class="label-form">Imagen de la marca</label>
                <div class="form-group">
                    <input type="file" class="form-control" name="image_file" required="required">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
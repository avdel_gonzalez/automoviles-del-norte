@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Marcas</h4>
        </div>
        <div class="card-body">
        <a href="{{ route('brands.create') }}" class="btn btn-primary">
            Crear nueva marca
        </a>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($brands as $brand)
                            <tr>
                                <td>{{ $brand->name }}</td>
                                <td>
                                    <img src="{{ asset($brand->brandImage->path . '/' . $brand->brandImage->file) }}" class="img-thumbnail" style="max-height: 80px !important;">
                                </td>
                                <td>
                                    <a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-primary">
                                        Editar
                                    </a>
                                    <a href="{{ route('brands.destroy', $brand->id) }}" class="btn btn-danger">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Nueva marca</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('brands.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="brand_id" value="{{ $brand->id }}">
                <label class="label-form">Nombre</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" required="required" placeholder="Nombre de la marca" value="{{ $brand->name }}">
                </div>
                <label class="label-form">Imagen de la marca</label><br />
                <small>Si no desea cambiarla, no es necesario que suba una nueva</small>
                <div class="form-group">
                    <input type="file" class="form-control" name="image_file">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
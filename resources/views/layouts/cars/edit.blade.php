@extends('layouts.app')

@section('styles')
    <style>
        #radioBtn .notActive{
            color: #3276b1;
            background-color: #fff !important;
        }
    </style>
@endsection

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Nuevo carro</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('cars.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="car_id" value="{{ $car->id }}">
                <label class="label-form">Marca</label>
                <div class="form-group">
                    <select class="form-control" name="brand_id">
                        <option value="0">Seleccione...</option>
                        @foreach($brands as $brand)
                            <option value="{{ $brand->id }}" @if($car->brand_id == $brand->id) selected="selected" @endif>{{ $brand->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label class="label-form">Versión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="version" required="required" placeholder="Versión del vehículo" value="{{ $car->version }}">
                </div>
                <label class="label-form">Modelo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="model" required="required" placeholder="Modelo" value="{{ $car->model }}">
                </div>
                <label class="label-form">Combustible</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="fuel" required="required" placeholder="Combustible del vehículo" value="{{ $car->fuel }}">
                </div>
                <label class="label-form">Kilometraje</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="mileage" required="required" placeholder="Kilometraje del vehículo SIN PUNTOS NI COMAS" value="{{ $car->mileage }}">
                </div>
                <label class="label-form">Dirección</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="direction" required="required" placeholder="Dirección del vehículo" value="{{ $car->direction }}">
                </div>
                <label class="label-form">Transmisión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="transmission" required="required" placeholder="Dirección del vehículo" value="{{ $car->transmission }}">
                </div>
                <label class="label-form">Valor</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="cost" required="required" placeholder="Valor del vehículo SIN PUNTOS NI COMAS" value="{{ $car->cost }}">
                </div>
                <label class="label-form">Cilindraje</label>
                <div class="form-group">
                <input type="number" class="form-control" name="cylinder_capacity" required="required" placeholder="Cilindrada del vehículo, SIN PUNTOS NI COMAS" value="{{ $car->cylinder_capacity }}">
                </div>
                <label class="label-form">Puertas</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="doors" required="required" placeholder="# puertas" value="{{ $car->doors }}">
                </div>
                <label class="label-form">Placa</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="license_plate" required="required" placeholder="# placa" value="{{ $car->license_plate }}">
                </div>
                <label class="label-form">Tracción del Vehículo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="traction" required="required" placeholder="Control de tracción (Ejemplo: 4x2, 4x4)" value="{{ $car->traction }}">
                </div>
                <label class="label-form">Tipo de carrocería</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="body_type" required="required" placeholder="Tipo de carrocería" value="{{ $car->body_type }}">
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="airbag" class="control-label text-right label-form">Airbag</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->airbag == 'Si') active @else notActive @endif" data-toggle="airbag" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->airbag == 'No') active @else notActive @endif" data-toggle="airbag" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="airbag" id="airbag" @if($car->airbag == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="alarm" class="control-label text-right label-form">Alarma</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->alarm == 'Si') active @else notActive @endif" data-toggle="alarm" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->alarm == 'No') active @else notActive @endif" data-toggle="alarm" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="alarm" id="alarm" @if($car->airbag == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="stability_control" class="control-label text-right label-form">Control de estabilidad</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->stability_control == 'Si') active @else notActive @endif" data-toggle="stability_control" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->stability_control == 'No') active @else notActive @endif" data-toggle="stability_control" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="stability_control" id="stability_control" @if($car->stability_control == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="rear_defroster" class="control-label text-right label-form">Desempañador trasero</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->rear_defroster == 'Si') active @else notActive @endif" data-toggle="rear_defroster" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->rear_defroster == 'No') active @else notActive @endif" data-toggle="rear_defroster" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="rear_defroster" id="rear_defroster" @if($car->rear_defroster == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="abs_brakes" class="control-label text-right label-form">Frenos ABS</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->abs_brakes == 'Si') active @else notActive @endif" data-toggle="abs_brakes" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->abs_brakes == 'No') active @else notActive @endif" data-toggle="abs_brakes" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="abs_brakes" id="abs_brakes" @if($car->abs_brakes == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="air_conditioning" class="control-label text-right label-form">Aire acondicionado</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->air_conditioning == 'Si') active @else notActive @endif" data-toggle="air_conditioning" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->air_conditioning == 'No') active @else notActive @endif" data-toggle="air_conditioning" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="air_conditioning" id="air_conditioning" @if($car->air_conditioning == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="electric_mirrors" class="control-label text-right label-form">Espejos eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->electric_mirrors == 'Si') active @else notActive @endif" data-toggle="electric_mirrors" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->electric_mirrors == 'No') active @else notActive @endif" data-toggle="electric_mirrors" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="electric_mirrors" id="electric_mirrors" @if($car->electric_mirrors == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sliding_roof" class="control-label text-right label-form">Techo corredizo</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->sliding_roof == 'Si') active @else notActive @endif" data-toggle="sliding_roof" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->sliding_roof == 'No') active @else notActive @endif" data-toggle="sliding_roof" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="sliding_roof" id="sliding_roof" @if($car->sliding_roof == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="rain_sensor" class="control-label text-right label-form">Sensor de lluvia</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->rain_sensor == 'Si') active @else notActive @endif" data-toggle="rain_sensor" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->rain_sensor == 'No') active @else notActive @endif" data-toggle="rain_sensor" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="rain_sensor" id="rain_sensor" @if($car->rain_sensor == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="electric_glasses" class="control-label text-right label-form">Vidrios eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->electric_glasses == 'Si') active @else notActive @endif" data-toggle="electric_glasses" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->electric_glasses == 'No') active @else notActive @endif" data-toggle="electric_glasses" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="electric_glasses" id="electric_glasses" @if($car->electric_glasses == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="power_seats" class="control-label text-right label-form">Asientos eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->power_seats == 'Si') active @else notActive @endif" data-toggle="power_seats" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->power_seats == 'No') active @else notActive @endif" data-toggle="power_seats" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="power_seats" id="power_seats" @if($car->power_seats == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="central_locking_of_doors" class="control-label text-right label-form">Cierre centralizado de puertas</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->central_locking_of_doors == 'Si') active @else notActive @endif" data-toggle="central_locking_of_doors" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->central_locking_of_doors == 'No') active @else notActive @endif" data-toggle="central_locking_of_doors" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="central_locking_of_doors" id="central_locking_of_doors" @if($car->central_locking_of_doors == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="air_conditioner" class="control-label text-right label-form">Climatizador</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->air_conditioner == 'Si') active @else notActive @endif" data-toggle="air_conditioner" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->air_conditioner == 'No') active @else notActive @endif" data-toggle="air_conditioner" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="air_conditioner" id="air_conditioner" @if($car->air_conditioner == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mp3_player" class="control-label text-right label-form">Reproductor MP3</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->mp3_player == 'Si') active @else notActive @endif" data-toggle="mp3_player" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->mp3_player == 'No') active @else notActive @endif" data-toggle="mp3_player" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="mp3_player" id="mp3_player" @if($car->mp3_player == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="bluetooth" class="control-label text-right label-form">Bluetooth</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->bluetooth == 'Si') active @else notActive @endif" data-toggle="bluetooth" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->bluetooth == 'No') active @else notActive @endif" data-toggle="bluetooth" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="bluetooth" id="bluetooth" @if($car->bluetooth == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="dvd" class="control-label text-right label-form">DVD</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm @if($car->dvd == 'Si') active @else notActive @endif" data-toggle="dvd" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm @if($car->dvd == 'No') active @else notActive @endif" data-toggle="dvd" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="dvd" id="dvd" @if($car->dvd == 'Si') value="Y" @else value="N" @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="usb_entry" class="control-label text-right label-form">Entrada USB</label>
                    <div class="">
                        <div class="input-group">
                            <div id="radioBtn" class="btn-group">
                                <a class="btn btn-primary btn-sm @if($car->usb_entry == 'Si') active @else notActive @endif" data-toggle="usb_entry" data-title="Y">Sí</a>
                                <a class="btn btn-primary btn-sm @if($car->usb_entry == 'No') active @else notActive @endif" data-toggle="usb_entry" data-title="N">No</a>
                            </div>
                            <input type="hidden" name="usb_entry" id="usb_entry" @if($car->usb_entry == 'Si') value="Y" @else value="N" @endif>
                        </div>
                    </div>
                </div>
                <label class="label-form">Ubicación del Vehículo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="vehicle_ubication" required="required" placeholder="Ubicación del Vehículo" value="{{ $car->vehicle_ubication }}">
                </div>
                <label class="label-form">Descripción</label>
                <div class="form-group">
                    <textarea class="form-control" name="description" required="required">{{ $car->description }}</textarea>
                </div>
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($car->carImages as $i => $image)
                            <div class="carousel-item @if($i == 0) active @endif">
                                <img class="d-block w-100 internal-image-view" src="{{ asset('' . $image->path . '/' . $image->file)}} " alt="...">
                                <div class="carousel-caption d-none d-md-block carousel-caption-text">
                                    <a href="{{ route('cars.dropImage', $image->id) }}" class="btn btn-danger">
                                        Eliminar esta imagen
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <label class="label-form">Imagen principal</label>
                <small>Si no desea añadir una, no es necesario que suba alguna</small>
                <div class="form-group">
                    <input type="file" class="form-control" name="main">
                </div>
                <label class="label-form">Imágenes del vehículo</label><br />
                <small>Si no desea añadir una, no es necesario que suba alguna</small>
                <div class="form-group">
                    <input type="file" class="form-control" name="image_files[]" multiple>
                </div>
                <label class="label-form">Video</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="youtube_video" placeholder="Link de Video YouTube" @if($car->youtube_video != null) value="{{ $car->youtube_video }}" @endif>
                </div>
                <label>Usuario responsable</label>
                <div class="form-group">
                    <select class="form-control" name="user_id">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" @if($user->id == $car->user_id) selected="selected" @endif>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        $(document).ready(function(){
            $('#radioBtn a').on('click', function(){
                var sel = $(this).data('title');
                var tog = $(this).data('toggle');
                $('#'+tog).prop('value', sel);
                
                $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
                $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
            });
        });
    </script>
@endsection
@extends('layouts.app')

@section('styles')
    <style>
        #radioBtn .notActive{
            color: #3276b1;
            background-color: #fff !important;
        }
    </style>
@endsection

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Nuevo carro</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('cars.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label class="label-form">Marca</label>
                <div class="form-group">
                    <select class="form-control" name="brand_id">
                        <option value="0">Seleccione...</option>
                        @foreach($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label class="label-form">Modelo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="model" required="required" placeholder="Modelo">
                </div>
                <label class="label-form">Versión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="version" required="required" placeholder="Versión del vehículo">
                </div>
                <label class="label-form">Combustible</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="fuel" required="required" placeholder="Combustible del vehículo">
                </div>
                <label class="label-form">Kilometraje</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="mileage" required="required" placeholder="Kilometraje del vehículo SIN PUNTOS NI COMAS">
                </div>
                <label class="label-form">Dirección</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="direction" required="required" placeholder="Dirección del vehículo">
                </div>
                <label class="label-form">Transmisión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="transmission" required="required" placeholder="Dirección del vehículo">
                </div>
                <label class="label-form">Valor</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="cost" required="required" placeholder="Valor del vehículo SIN PUNTOS NI COMAS">
                </div>
                <label class="label-form">Cilindraje</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="cylinder_capacity" required="required" placeholder="Cilindrada del vehículo, SIN PUNTOS NI COMAS">
                </div>
                <label class="label-form">Puertas</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="doors" required="required" placeholder="# puertas">
                </div>
                <label class="label-form">Placa</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="license_plate" required="required" placeholder="# placa">
                </div>
                <label class="label-form">Tracción del Vehículo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="traction" required="required" placeholder="Control de tracción (Ejemplo: 4x2, 4x4)">
                </div>
                <label class="label-form">Tipo de carrocería</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="body_type" required="required" placeholder="Tipo de carrocería">
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="airbag" class="control-label text-right label-form">Airbag</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="airbag" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="airbag" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="airbag" id="airbag" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="alarm" class="control-label text-right label-form">Alarma</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="alarm" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="alarm" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="alarm" id="alarm" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="stability_control" class="control-label text-right label-form">Control de estabilidad</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="stability_control" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="stability_control" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="stability_control" id="stability_control" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="rear_defroster" class="control-label text-right label-form">Desempañador trasero</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="rear_defroster" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="rear_defroster" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="rear_defroster" id="rear_defroster" value="Y">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="abs_brakes" class="control-label text-right label-form">Frenos ABS</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="abs_brakes" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="abs_brakes" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="abs_brakes" id="abs_brakes" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="air_conditioning" class="control-label text-right label-form">Aire acondicionado</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="air_conditioning" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="air_conditioning" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="air_conditioning" id="air_conditioning" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="electric_mirrors" class="control-label text-right label-form">Espejos eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="electric_mirrors" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="electric_mirrors" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="electric_mirrors" id="electric_mirrors" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sliding_roof" class="control-label text-right label-form">Techo corredizo</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="sliding_roof" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="sliding_roof" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="sliding_roof" id="sliding_roof" value="Y">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="rain_sensor" class="control-label text-right label-form">Sensor de lluvia</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="rain_sensor" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="rain_sensor" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="rain_sensor" id="rain_sensor" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="electric_glasses" class="control-label text-right label-form">Vidrios eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="electric_glasses" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="electric_glasses" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="electric_glasses" id="electric_glasses" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="power_seats" class="control-label text-right label-form">Asientos eléctricos</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="power_seats" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="power_seats" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="power_seats" id="power_seats" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="central_locking_of_doors" class="control-label text-right label-form">Cierre centralizado de puertas</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="central_locking_of_doors" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="central_locking_of_doors" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="central_locking_of_doors" id="central_locking_of_doors" value="Y">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="air_conditioner" class="control-label text-right label-form">Climatizador de puertas</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="air_conditioner" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="air_conditioner" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="air_conditioner" id="air_conditioner" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mp3_player" class="control-label text-right label-form">Reproductor MP3</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="mp3_player" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="mp3_player" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="mp3_player" id="mp3_player" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="bluetooth" class="control-label text-right label-form">Bluetooth</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="bluetooth" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="bluetooth" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="bluetooth" id="bluetooth" value="Y">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="dvd" class="control-label text-right label-form">DVD</label>
                        <div class="">
                            <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" data-toggle="dvd" data-title="Y">Sí</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="dvd" data-title="N">No</a>
                                </div>
                                <input type="hidden" name="dvd" id="dvd" value="Y">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="usb_entry" class="control-label text-right label-form">Entrada USB</label>
                    <div class="">
                        <div class="input-group">
                            <div id="radioBtn" class="btn-group">
                                <a class="btn btn-primary btn-sm active" data-toggle="usb_entry" data-title="Y">Sí</a>
                                <a class="btn btn-primary btn-sm notActive" data-toggle="usb_entry" data-title="N">No</a>
                            </div>
                            <input type="hidden" name="usb_entry" id="usb_entry" value="Y">
                        </div>
                    </div>
                </div>
                <label class="label-form">Ubicación del Vehículo</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="vehicle_ubication" required="required" placeholder="Ubicación del Vehículo">
                </div>
                <label class="label-form">Descripción</label>
                <div class="form-group">
                    <textarea class="form-control" name="description" required="required">Descripción</textarea>
                </div>
                <label class="label-form">Imagen principal</label>
                <div class="form-group">
                    <input type="file" class="form-control" name="main" required="required">
                </div>
                <label class="label-form">Imágenes del vehículo</label>
                <div class="form-group">
                    <input type="file" class="form-control" name="image_files[]" required="required" multiple>
                </div>
                <label class="label-form">Video</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="youtube_video" placeholder="Link de Video YouTube">
                </div>
                <label>Usuario responsable</label>
                <div class="form-group">
                    <select class="form-control" name="user_id">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        $(document).ready(function(){
            $('#radioBtn a').on('click', function(){
                var sel = $(this).data('title');
                var tog = $(this).data('toggle');
                $('#'+tog).prop('value', sel);
                
                $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
                $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
            });
        });
    </script>
@endsection
@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Carros</h4>
        </div>
        <div class="col-md-12 text-center">
            <form class="form-inline" action="{{ route('cars.index') }}" method="GET" role="search">
                <div class="input-group">
                    <select name="brand_id" class="form-control" aria-describedby="search">
                        <option value="0">Seleccionar marca</option>
                        @foreach($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group">
                    <select name="range_cost" class="form-control" aria-describedby="search">
                        <option value="0">Seleccionar rango de costo</option>
                        <option value="1-10">$1'000.000 a $10'000.000</option>
                        <option value="10-20">$10'000.000 a $20'000.000</option>
                        <option value="20-30">$20'000.000 a $30'000.000</option>
                        <option value="30-40">$30'000.000 a $40'000.000</option>
                        <option value="40-50">$40'000.000 a $50'000.000</option>
                        <option value="50-60">$50'000.000 a $60'000.000</option>
                        <option value="60-70">$60'000.000 a $70'000.000</option>
                        <option value="70-80">$70'000.000 a $80'000.000</option>
                        <option value="80-90">$80'000.000 a $90'000.000</option>
                        <option value="90-100">$90'000.000 a $100'000.000</option>
                        <option value="100-max">$100'000.000 en adelante</option>
                    </select>
                </div>
                <div class="input-group">
                    <select name="range_mileage" class="form-control" aria-describedby="search">
                        <option value="0">Seleccionar rango de kilometraje</option>
                        <option value="1-10">1.000 a 10.000</option>
                        <option value="10-20">10.000 a 20.000</option>
                        <option value="20-30">20.000 a 30.000</option>
                        <option value="30-40">30.000 a 40.000</option>
                        <option value="40-50">40.000 a 50.000</option>
                        <option value="50-60">50.000 a 60.000</option>
                        <option value="60-70">60.000 a 70.000</option>
                        <option value="70-80">70.000 a 80.000</option>
                        <option value="80-90">80.000 a 90.000</option>
                        <option value="90-100">90.000 a 100.000</option>
                        <option value="100-max">100.000 en adelante</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
        <div class="card-body">
        <a href="{{ route('cars.create') }}" class="btn btn-primary">
            Crear nuevo carro
        </a>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>Marca</th>
                        <th>Versión</th>
                        <th>Combustible</th>
                        <th>Kilometraje</th>
                        <th>Dirección</th>
                        <th>Transmisión</th>
                        <th>Costo</th>
                        <th>Usuario responsable</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($cars as $car)
                            <tr>
                                <td>{{ $car->brand->name }}</td>
                                <td>{{ $car->version }}</td>
                                <td>{{ $car->fuel }}</td>
                                <td>{{ $car->mileage }}</td>
                                <td>{{ $car->direction }}</td>
                                <td>{{ $car->transmission }}</td>
                                <td>${{ number_format($car->cost, 0, ',', '.') }}</td>
                                <td>{{ $car->user->name }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view-car-images" onclick="getImages('{{ $car->id }}')">
                                        Ver
                                    </button>
                                    <a href="{{ route('cars.edit', $car->id) }}" class="btn btn-primary">
                                        Editar
                                    </a>
                                    <a href="{{ route('cars.destroy', $car->id) }}" class="btn btn-danger">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-car-images" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Imágenes del vehículo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="car-images-carousel">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        function getImages(car_id){
            $('#car-images-carousel').empty();

            let url = window.location.origin + '/cars/getImages/' + car_id;
            $.getJSON(url, function(data){
                images_template = '';
                $.each(data, function(key, value){
                    (key == 0) ? class_div = 'carousel-item active' : class_div = 'carousel-item';
                    images_template += `
                        <div class="${class_div}">
                            <img class="d-block w-100" src="${window.location.origin}/${value.path}/${value.file}" alt="..." style="max-height: 200px;">
                        </div>
                    `;
                });
                let template = `
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            ${images_template}
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                `;
                console.log(template);
                $('#car-images-carousel').append(template);
            });
        }
    </script>
@endsection
<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('img/sidebar-1.jpg') }}">
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
            Automóviles del norte
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('brands.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>Marcas</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('cars.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>Carros</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('polarized.index') }}">
                    <i class="material-icons">art_track</i>
                    <p>Polarizados</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('seating.index') }}">
                    <i class="material-icons">art_track</i>
                    <p>Tapicería</p>
                </a>
            </li>
            @if(Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('users.index') }}">
                        <i class="material-icons">person</i>
                        <p>Usuarios</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('company.index') }}">
                        <i class="material-icons">content_paste</i>
                        <p>Compañía</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
@extends('layouts.app')

@section('styles')
    <style>
        body {
            /*background-color:#1d1d1d !important;*/
            font-family: "Asap", sans-serif;
            color:#989898;
            margin:10px;
            font-size:16px;
        }

        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }


        .green{
            background-color:#6fb936;
        }
        
        .thumb{
            margin-bottom: 30px;
        }
                    
        .page-top{
            margin-top:85px;
        }

            
        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }
                    
            
        .transition {
            -webkit-transform: scale(1.2); 
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }

        .modal-header {
            border-bottom: none;
        }

        .modal-title {
            color:#000;
        }
                
        .modal-footer{
            display:none;  
        }

    </style>
@endsection

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Galería de Polarizados</h4>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-image">
                Agregar contenido
            </button>
        </div>
    </div>

    <div class="container page-top">
        <div class="row">
            @foreach($images as $image)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a href="{{ asset($image->path . '/' . $image->file) }}?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                        <img src="{{ asset($image->path . '/' . $image->file) }}?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid "  alt="">
                    </a>
                    <a href="{{ route('polarized.destroy', $image->id) }}" class="btn btn-danger">Eliminar imagen</a>
                </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="add-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('polarized.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body" id="car-images-carousel">
                        <div class="form-group">
                            <input type="file" class="form-control" name="image_files[]" required="required" multiple accept="image/*">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script>
        $(document).ready(function(){
          $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function(){
            
        		$(this).addClass('transition');
        	}, function(){

        		$(this).removeClass('transition');
        	});
        });
    </script>
@endsection
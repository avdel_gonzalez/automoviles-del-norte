@extends('layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <h2>
            Editar Compañía: {{ $company->name }}
        </h2>
        <div class="container">
            <form action="{{ route('company.update') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="company_id" value="{{ $company->id }}">
            <label class="label-form">Nombre</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" required="required" value="{{ $company->name }}">
                </div>
                <label class="label-form">Descripción</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="description" required="required" value="{{ $company->description }}">
                </div>
                <label class="label-form">Información de Polarizados</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="polarized_info" required="required" value="{{ $company->polarized_info }}">
                </div>
                <label class="label-form">Información de Venta de automóviles</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="carshop_info" required="required" value="{{ $company->carshop_info }}">
                </div>
                <label class="label-form">Información de Tapicerías</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="upholstery_info" required="required" value="{{ $company->upholstery_info }}">
                </div>
                <label class="label-form">Objetivos</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="objectives" value="{{ $company->objectives }}">
                </div>
                <label class="label-form">Misión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="mission" value="{{ $company->mission }}">
                </div>
                <label class="label-form">Visión</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="vission" value="{{ $company->vission }}">
                </div>
                <label class="label-form">Facebook (link)</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="facebook" value="{{ $company->facebook }}">
                </div>
                <label class="label-form">Instagram (link)</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="instagram" value="{{ $company->instagram }}">
                </div>
                <label class="label-form">WhatsApp</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="whatsapp" value="{{ $company->whatsapp }}">
                </div>
                <label class="label-form">Teléfono de contacto</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="phone_number" value="{{ $company->phone_number }}">
                </div>
                <label class="label-form">Pie de página</label>
                <div class="form-group">
                    <textarea class="form-control" name="footer_text" rows="3">{{ $company->footer_text }}</textarea>
                </div>
                <label class="label-form">Imagen de fondo</label>
                <div class="form-group">
                    <input type="file" name="background_image" accept="image/png, image/jpg, image,jpeg">
                </div>
                <label class="label-form">Imagen de Venta de carros</label>
                <div class="form-group">
                    <input type="file" name="carshop_image" accept="image/png, image/jpg, image,jpeg">
                </div>
                <label class="label-form">Imagen de Polarizados</label>
                <div class="form-group">
                    <input type="file" name="polarized_image" accept="image/png, image/jpg, image,jpeg">
                </div>
                <label class="label-form">Imagen de Tapicerías</label>
                <div class="form-group">
                    <input type="file" name="upholstery_image" accept="image/png, image/jpg, image,jpeg">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection

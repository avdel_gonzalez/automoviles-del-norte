@extends('layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <a href="{{ route('company.edit', $company->id) }}" class="btn btn-warning">
            Editar información de Compañía
        </a>
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Compañía: {{ $company->name }}</h4>
                <p class="card-category">Información de Compañía</p>
            </div>
            <div class="card-body">
                <strong>Nombre: </strong> {{ $company->name }}<br />
                <strong>Descripción: </strong> {{ $company->description }}<br />
                <strong>Objetivos: </strong> {{ $company->objectives }}<br />
                <strong>Polarizados: </strong> {{ $company->polarized_info }}<br />
                <strong>Tapicerías: </strong> {{ $company->upholstery_info }}<br />
                <strong>Venta de carros: </strong> {{ $company->carshop_info }}<br />
                <strong>Misión: </strong> {{ $company->mission }}<br />
                <strong>Visión: </strong> {{ $company->vission }}<br />
                <strong>Facebook: </strong> {{ $company->facebook }}<br />
                <strong>Instagram: </strong> {{ $company->instagram }}<br />
                <strong>WhatsApp: </strong> {{ $company->whatsapp }}<br />
                <strong>Teléfono de contacto: </strong> {{ $company->phone_number }}<br />
                <strong>Texto de pie de página: </strong> {{ $company->footer_text }}<br />
            </div>
        </div>
        <div class="card card-plain">
            <div class="card-header card-header-info">
                <h4 class="card-title mt-0">Imagen de fondo</h4>
            </div>
            <div class="card-body">
                <img src="{{ $company->background_image }}" class="img-fluid">
            </div>
        </div>
        <div class="card card-plain">
            <div class="card-header card-header-info">
                <h4 class="card-title mt-0">Imagen de Venta de carros</h4>
            </div>
            <div class="card-body">
                <img src="{{ $company->carshop_image }}" class="img-fluid">
            </div>
        </div>
        <div class="card card-plain">
            <div class="card-header card-header-info">
                <h4 class="card-title mt-0">Imagen de Polarizados</h4>
            </div>
            <div class="card-body">
                <img src="{{ $company->polarized_image }}" class="img-fluid">
            </div>
        </div>
        <div class="card card-plain">
            <div class="card-header card-header-info">
                <h4 class="card-title mt-0">Imagen de Tapicerías</h4>
            </div>
            <div class="card-body">
                <img src="{{ $company->upholstery_image }}" class="img-fluid">
            </div>
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection

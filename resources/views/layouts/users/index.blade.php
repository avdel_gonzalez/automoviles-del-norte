@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Marcas</h4>
        </div>
        <div class="card-body">
        <a href="{{ route('users.create') }}" class="btn btn-primary">
            Crear nuevo usuario
        </a>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>Nombre</th>
                        <th>E-mail</th>
                        <th>Celular</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->celphone_number }}</td>
                                <td>
                                    @if($user->role == 'admin')
                                        Administrador
                                    @else
                                        Regular
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">
                                        Editar
                                    </a>
                                    <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
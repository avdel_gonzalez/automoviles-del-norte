@extends('layouts.app')

@section('content')
    <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0">Nuevo usuario</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('users.update') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <label class="label-form">Nombre</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" required="required" placeholder="Nombre del usuario" value="{{ $user->name }}">
                </div>
                <label class="label-form">E-mail</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" required="required" placeholder="E-mail del usuario" value="{{ $user->email }}">
                </div>
                <label class="label-form">Contraseña</label>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="*****">
                </div>
                <label class="label-form">Celular</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="celphone_number" required="required" placeholder="Línea WhatsApp" value="{{ $user->celphne_number }}">
                </div>
                <label class="label-form">Rol</label>
                <div class="form-group">
                    <select name="role" class="form-control">
                        <option value="0">Seleccione...</option>
                        <option value="admin" @if($user->role == 'admin') selected="selected" @endif>Administrador</option>
                        <option value="regular" @if($user->role == 'regular') selected="selected" @endif>Regular</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 'cars';

    protected $fillable = [
        'version',
        'fuel',
        'mileage',
        'direction',
        'transmission',
        'cost',
        'brand_id',
        'model',
        'cylinder_capacity',
        'doors',
        'license_plate',
        'traction',
        'body_type',
        'airbag',
        'alarm',
        'stability_control',
        'rear_defroster',
        'abs_brakes',
        'air_conditioning',
        'electric_mirrors',
        'sliding_roof',
        'rain_sensor',
        'electric_glasses',
        'power_seats',
        'central_locking_of_doors',
        'air_conditioner',
        'mp3_player',
        'bluetooth',
        'dvd',
        'usb_entry',
        'vehicle_ubication',
        'description',
        'youtube_video',
        'user_id'
    ];

    public function brand(){
        return $this->belongsTo('App\Brand');
    }

    public function carImages(){
        return $this->hasMany('App\CarImage');
    }

    public function scopeCarFilter($query, $brand_id, $range_cost, $range_mileage){
        if(($brand_id != '' && is_numeric($brand_id) && $brand_id != '0') && ($range_cost == null || $range_cost == '0') && ($range_mileage == null || $range_mileage == '0')){
            $query->where('brand_id', $brand_id);
        } elseif(($brand_id != '' && is_numeric($brand_id) && $brand_id != '0') && ($range_cost != '' && $range_cost != '0') && ($range_mileage == null || $range_mileage == '0')){
            $range = explode('-', $range_cost);
            $query->where('brand_id', $brand_id)
                ->whereBetween('cost', array($range[0] . '000000', $range[1] . '000000'));
        } elseif (($brand_id != '' && is_numeric($brand_id) && $brand_id != '0') && ($range_cost != '' && $range_cost != '0') && ($range_mileage != '' && $range_mileage != '0')){
            $cost_range = explode('-', $range_cost);
            $mileage_range = explode('-', $range_mileage);
            $query->where('brand_id', $brand_id)
                ->whereBetween('cost', array($cost_range[0] . '000000', $cost_range[1] . '000000'))
                ->whereBetween('mileage', array($mileage_range[0] . '000000', $mileage_range[1] . '000000'));
        } elseif(($brand_id == '0' || $brand_id == null) && ($range_cost != '' && $range_cost != '0') && ($range_mileage != '' && $range_mileage != '0')){
            $cost_range = explode('-', $range_cost);
            $mileage_range = explode('-', $range_mileage);
            $query->whereBetween('cost', array($cost_range[0] . '000000', $cost_range[1] . '000000'))
                ->whereBetween('mileage', array($mileage_range[0] . '000000', $mileage_range[1] . '000000'));
        } elseif(($brand_id == '0' || $brand_id == null) && ($range_cost == null || $range_cost == '0') && ($range_mileage != '' && $range_mileage != '0')){
            $mileage_range = explode('-', $range_mileage);
            $query->whereBetween('mileage', array($mileage_range[0] . '000000', $mileage_range[1] . '000000'));
        } elseif(($brand_id == '0' || $brand_id == null) && ($range_cost != '' && $range_cost != '0') && ($range_mileage == null || $range_mileage == '0')){
            $cost_range = explode('-', $range_cost);
            $query->whereBetween('cost', array($cost_range[0] . '000000', $cost_range[1] . '000000'));
        }
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company_info';

    protected $fillable = [
        'name',
        'description',
        'objectives',
        'mission',
        'vission',
        'polarized_info',
        'carshop_info',
        'facebook',
        'instagram',
        'whatsapp',
        'phone_number',
        'footer_text',
        'background_image',
        'upholstery_info',
        'carshop_image',
        'polarized_image',
        'upholstery_image',
    ];
}

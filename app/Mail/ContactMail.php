<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $phone, $user_message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->user_message = $user_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('automovilesdelnortesas@gmail.com', 'Automóviles del norte')
                    ->subject('Nuevo mensaje de contacto automovilesdelnorte.com')
                    ->with([
                        'name' => $this->name,
                        'email' => $this->email,
                        'phone' => $this->phone,
                        'user_message' => $this->user_message,
                    ])
                    ->view('emails.contact');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AskByCarMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($car_reference, $user_name, $user_email, $user_message, $car_url)
    {
        $this->car_reference = $car_reference;
        $this->user_name = $user_name;
        $this->user_email = $user_email;
        $this->user_message = $user_message;
        $this->car_url = $car_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('automovilesdelnortesas@gmail.com', 'Automóviles del norte')
                    ->subject('Usuario pregunta por un carro en automovilesdelnorte.com')
                    ->with([
                        'car_reference' => $this->car_reference,
                        'user_name' => $this->user_name,
                        'user_email' => $this->user_email,
                        'user_message' => $this->user_message,
                        'car_url' => $this->car_url,
                    ])
                    ->view('emails.ask_by_car');
    }
}

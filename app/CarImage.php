<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarImage extends Model
{
    protected $table = 'car_images';

    protected $fillable = [
        'path',
        'file',
        'car_id',
        'main'
    ];

    public function car(){
        return $this->belongsTo('App\Car');
    }
}

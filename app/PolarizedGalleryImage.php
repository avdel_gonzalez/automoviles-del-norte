<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolarizedGalleryImage extends Model
{
    protected $table = 'polarized_gallery_images';

    protected $fillable = [
        'path',
        'file'
    ];
}

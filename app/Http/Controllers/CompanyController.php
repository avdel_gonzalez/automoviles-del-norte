<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use SweetAlert;

class CompanyController extends Controller
{
    public function index(){
        $company = Company::find('1');
        return view('layouts.company.index')->with('company', $company);
    }

    public function edit($id){
        $company = Company::find($id);
        return view('layouts.company.edit')->with('company', $company);
    }

    public function update(Request $request){
        $company = Company::find($request->company_id);
        $background_image_name = '';
        $carshop_image_name = '';
        $polarized_image_name = '';
        $upholstery_image_name = '';
        if(isset($request->background_image)){
            if(!file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'))){
                if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'), 0755, true)){
                    alert()->error('No se puede crear el directorio para la imagen')->autoclose(5000);
                    return back();
                }
            }
            if($company->background_image != null){
                if(file_exists(public_path($company->background_image))){
                    unlink(public_path($company->background_image));
                }
                $company->background_image = null;
            }
            $image = $request->background_image;
            $background_image_name = 'header-bg.' . $image->getClientOriginalExtension();
            $background_ref_image_info = 'uploads' . DIRECTORY_SEPARATOR . 'company_info' . DIRECTORY_SEPARATOR . $background_image_name;
            $image_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info');
            $image->move($image_path, $background_image_name);
        }
        if(isset($request->carshop_image)){
            if(!file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'))){
                if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'), 0755, true)){
                    alert()->error('No se puede crear el directorio para la imagen')->autoclose(5000);
                    return back();
                }
            }
            if($company->carshop_image != null){
                if(file_exists(public_path($company->carshop_image))){
                    unlink(public_path($company->carshop_image));
                }
                $company->carshop_image = null;
            }
            $image = $request->carshop_image;
            $carshop_image_name = 'carshop.' . $image->getClientOriginalExtension();
            $carshop_ref_image_info = 'uploads' . DIRECTORY_SEPARATOR . 'company_info' . DIRECTORY_SEPARATOR . $carshop_image_name;
            $image_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info');
            $image->move($image_path, $carshop_image_name);
        }
        if(isset($request->polarized_image)){
            if(!file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'))){
                if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'), 0755, true)){
                    alert()->error('No se puede crear el directorio para la imagen')->autoclose(5000);
                    return back();
                }
            }
            if($company->polarized_image != null){
                if(file_exists(public_path($company->polarized_image))){
                    unlink(public_path($company->polarized_image));
                }
                $company->polarized_image = null;
            }
            $image = $request->polarized_image;
            $polarized_image_name = 'polarized.' . $image->getClientOriginalExtension();
            $polarized_ref_image_info = 'uploads' . DIRECTORY_SEPARATOR . 'company_info' . DIRECTORY_SEPARATOR . $polarized_image_name;
            $image_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info');
            $image->move($image_path, $polarized_image_name);
        }
        if(isset($request->upholstery_image)){
            if(!file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'))){
                if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info'), 0755, true)){
                    alert()->error('No se puede crear el directorio para la imagen')->autoclose(5000);
                    return back();
                }
            }
            if($company->upholstery_image != null){
                if(file_exists(public_path($company->upholstery_image))){
                    unlink(public_path($company->upholstery_image));
                }
                $company->upholstery_image = null;
            }
            $image = $request->upholstery_image;
            $upholstery_image_name = 'upholstery.' . $image->getClientOriginalExtension();
            $upholstery_ref_image_info = 'uploads' . DIRECTORY_SEPARATOR . 'company_info' . DIRECTORY_SEPARATOR . $upholstery_image_name;
            $image_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'company_info');
            $image->move($image_path, $upholstery_image_name);
        }
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'objectives' => $request->objectives,
            'mission' => $request->mission,
            'vission' => $request->vission,
            'polarized_info' => $request->polarized_info,
            'carshop_info' => $request->carshop_info,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'whatsapp' => $request->whatsapp,
            'phone_number' => $request->phone_number,
            'footer_text' => $request->footer_text,
            'background_image' => ($background_image_name != '') ? $background_ref_image_info : $company->background_image,
            'carshop_image' => ($carshop_image_name != '') ? $carshop_ref_image_info : $company->carshop_image,
            'polarized_image' => ($polarized_image_name != '') ? $polarized_ref_image_info : $company->polarized_image,
            'upholstery_image' => ($upholstery_image_name != '') ? $upholstery_ref_image_info : $company->upholstery_image,
            'upholstery_info' => $request->upholstery_info,
        ];
        $company->fill($data);
        $company->save();
        alert()->success('La información de la compañía "' . $request->name . '" ha sido editada exitosamente')->autoclose(5000);
        return redirect()->route('company.index');
    }
}

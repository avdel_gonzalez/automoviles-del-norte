<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SweetAlert;
use App\User;

class UsersController extends Controller
{
    public function index(){
        $users = User::orderBy('id', 'asc')->get();
        return view('layouts.users.index')->with(compact('users'));
    }

    public function create(){
        return view('layouts.users.create');
    }

    public function store(Request $request){
        $messages = [
            'name.required' => 'El nombre del usuario es obligatorio',
            'name.string' => 'El nombre del usuario debe ser una cadena de texto',
            'email.required' => 'El correo electrónico es obligatorio',
            'email.unique' => 'El email ya está registrado en la base de datos',
            'password.required' => 'Debe asignar una contraseña para el usuario',
            'role.required' => 'Debe especificar un rol para el usuario',
            'role.in' => 'Debe asignar un rol para el usuario',
            'celphone_number.required' => 'Debe proporcionar un número de celular',
        ];

        $rules = [
            'name' => 'required|string',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'celphone_number' => 'required',
            'role' => 'required|in:admin,regular'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'celphone_number' => $request->celphone_number,
                'role' => $request->role
            ];
            $user = new User($data);
            $user->save();
            
            alert()->success('Se ha creado al usuario "' . $request->name . '" exitosamente', 'Perfecto!')->autoclose(5000);
            return redirect()->route('users.index');
        }
    }

    public function edit($id){
        $user = User::find($id);
        return view('layouts.users.edit')->with(compact('user'));
    }

    public function update(Request $request){
        $messages = [
            'user_id.required' => 'El id del usuario es obligatorio',
            'user_id.numeric' => 'El id del usuario debe ser un valor numérico',
            'user_id.exists' => 'El id del usuario no está registrado en la base de datos',
            'name.required' => 'El nombre del usuario es obligatorio',
            'name.string' => 'El nombre del usuario debe ser una cadena de texto',
            'email.required' => 'El correo electrónico es obligatorio',
            'role.required' => 'Debe especificar un rol para el usuario',
            'role.in' => 'Debe asignar un rol para el usuario',
            'celphone_number.required' => 'Debe proporcionar un número de celular',
        ];

        $rules = [
            'user_id' => 'required|numeric|exists:users,id',
            'name' => 'required|string',
            'email' => 'required',
            'celphone_number' => 'required',
            'role' => 'required|in:admin,regular'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            $same_email = User::where([
                ['id', '!=', $request->user_id],
                ['email', '=', $request->email]
            ])->first();
            if($same_email == null){
                $user = User::find($request->user_id);
                $data = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => (isset($request->password)) ? bcrypt($request->password) : $user->password,
                    'celphone_number' => $request->celphone_number,
                    'role' => $request->role
                ];
                $user->fill($data);
                $user->save();

                alert()->success('Se ha editado al usuario "' . $user->name . '" exitosamente', 'Perfecto!')->autoclose(5000);
                return redirect()->route('users.index');
            } else {
                alert()->error('El email ya está registrado en la base de datos', 'Ups!')->autoclose(5000);
                return back();
            }
        }
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        alert()->success('Se ha eliminado al usuario "' . $user->name . '" exitosamente', 'Perfecto!')->autoclose(5000);
        return redirect()->route('users.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Car;
use App\Brand;
use App\CarImage;
use App\PolarizedGalleryImage;
use App\SeatingGalleryImage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use App\Mail\AskByCarMail;
use Illuminate\Support\Facades\Validator;
use SweetAlert;

class FrontController extends Controller
{
    public function index(){
        $company = Company::find('1');
        $polarized_images = PolarizedGalleryImage::orderBy('id', 'desc')->take(6)->get();
        $seating_images = SeatingGalleryImage::orderBy('id', 'desc')->take(6)->get();
        $cars = Car::orderBy('id', 'desc')->take(6)->get();
        foreach($cars as $car){
            $get_main_image = CarImage::where([
                ['car_id',  '=', $car->id],
                ['main', '=', 'Yes']
            ])->first();
            $car->mainImage = $get_main_image;
        }
        return view('front.index')->with(compact('company', 'polarized_images', 'seating_images', 'cars'));
    }

    public function cars(Request $request){
        $company = Company::find('1');
        $brands = Brand::orderBy('id', 'asc')->get();
        $cars = Car::CarFilter($request->get('brand_id'), $request->get('range_cost'), $request->get('range_mileage'))->with(['carImages', 'brand'])->get();
        foreach($cars as $car){
            $get_main_image = CarImage::where([
                ['car_id',  '=', $car->id],
                ['main', '=', 'Yes']
            ])->first();
            $car->mainImage = $get_main_image;
        }
        return view('front.cars')->with(compact('cars', 'brands', 'company'));
    }

    public function carDetails($id){
        $company = Company::find('1');
        $car = Car::where('id', $id)->with(['brand', 'carImages', 'user'])->first();
        return view('front.car_details')->with(compact('car', 'company'));
    }

    public function polarizedGallery(){
        $company = Company::find('1');
        $images = PolarizedGalleryImage::orderBy('id', 'desc')->get();
        return view('front.polarized_gallery')->with(compact('images', 'company'));
    }

    public function seatingGallery(){
        $company = Company::find('1');
        $images = SeatingGalleryImage::orderBy('id', 'desc')->get();
        return view('front.seating_gallery')->with(compact('images', 'company'));
    }

    public function contactForm(){
        $company = Company::find('1');
        return view('front.contact_form')->with(compact('company'));
    }

    public function sendMail(Request $request){
        $messages = [
            'name.required' => 'Por favor ingrese su nombre',
            'email.required' => 'Por favor ingrese su email',
            'phone.required' => 'Por favor ingrese su número de contacto',
            'user_message.required' => 'Por favor ingrese su mensaje' 
        ];

        $rules = [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_message' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            Mail::to('automovilesdelnortesas@gmail.com')->send(new ContactMail($request->name, $request->email, $request->phone, $request->user_message));
            alert()->success('El mensaje se envió exitosamente', '¡Gracias por contactarnos!')->autoclose(5000);
            return view('front.contact_form');
        }
    }

    public function askByCar(Request $request){
        $messages = [
            'car_id.required' => 'Id del carro es obligatorio',
            'car_id.numeric' => 'Id del carro en formato inválido',
            'car_id.exists' => 'Id del carro no existe en la base de datos',
            'name.required' => 'El nombre es obligatorio',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email no es válido',
            'user_message.required' => 'Debe escribir un mensaje'
        ];

        $rules = [
            'car_id' => 'required|numeric|exists:cars,id',
            'name' => 'required',
            'email' => 'required|email',
            'user_message' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            $car = Car::where('id', $request->car_id)->with(['brand'])->first();
            $car_reference = $car->brand->name . ': ' . $car->version . ' - ' . $car->model;
            $car_url = $request->getSchemeAndHttpHost() . '/view_cars/carDetails/' . $car->id;
            $user_name = $request->name;
            $user_email = $request->email;
            $user_message = $request->user_message;
            Mail::to('automovilesdelnortesas@gmail.com')->send(new AskByCarMail($car_reference, $user_name, $user_email, $user_message, $car_url));
            alert()->success('El mensaje se envió exitosamente', '¡Gracias por contactarnos!')->autoclose(5000);
            return back();
        }
    }
}

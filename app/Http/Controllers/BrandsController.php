<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SweetAlert;
use App\Brand;
use App\BrandImage;

class BrandsController extends Controller
{
    public function index(){
        $brands = Brand::orderBy('id', 'asc')->get();
        return view('layouts.brands.index')->with(compact('brands'));
    }

    public function create(){
        return view('layouts.brands.create');
    }

    public function store(Request $request){
        $messages = [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre es obligatorio',
            'image_file.required' => 'El archivo es obligatorio',
            'image_file.mime' => 'El archivo es obligatorio',
        ];

        $rules = [
            'name' => 'required|unique:brands,name',
            'image_file' => 'required|mimes:jpg,png'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return redirect()->route('brands.index');
        } else {
            $data = [
                'name' => $request->name,
            ];
            $brand = new Brand($data);
            $brand->save();

            if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . $brand->id), 0755, true)){
                alert()->error('No fue posible crear el directorio')->autoclose(5000);
                return back();
            } else {
                $attachment = $request->image_file;
                $name = 'attachment-' . time() . $attachment->getClientOriginalName();
                $path = public_path('uploads' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . $brand->id);
                $ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . $brand->id;
                $attachment->move($path, $name);
                $data_image = [
                    'path' => $ref_path,
                    'file' => $name,
                    'brand_id' => $brand->id
                ];
                $image = new BrandImage($data_image);
                $image->save();
                
                alert()->success('Se creó la marca "' . $request->name . '" exitosamente', 'Perfecto')->autoclose(5000);
                return redirect()->route('brands.index');
            }
        }
    }

    public function edit($id){
        $brand = Brand::find($id);
        return view('layouts.brands.edit')->with(compact('brand'));
    }

    public function update(Request $request){
        $messages = [
            'brand_id.required' => 'El id de la marca es obligatorio',
            'brand_id.numeric' => 'El id de la marca debe ser un valor numérico',
            'brand_id.exists' => 'Esta marca no existe en la base de datos',
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'El nombre es obligatorio',
        ];

        $rules = [
            'brand_id' => 'required|numeric|exists:brands,id',
            'name' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        
        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return redirect()->route('brands.index');
        } else {
            $exists = Brand::where([
                ['name', '=', $request->name],
                ['id', '!=', $request->brand_id]
            ])->first();
            if($exists != null){
                alert()->error('Esta marca ya está registrada en la base de datos', 'Ups!')->autoclose(5000);
                return back();
            } else {
                $brand = Brand::find($request->brand_id);
                if(isset($request->image_file)){
                    $brand_image = BrandImage::where('brand_id', $brand->id)->first();
                    unlink(public_path($brand_image->path . DIRECTORY_SEPARATOR . $brand_image->file));
                    $attachment = $request->image_file;
                    $name = 'attachment-' . time() . $attachment->getClientOriginalName();
                    $path = public_path('uploads' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . $brand->id);
                    $ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . $brand->id;
                    $attachment->move($path, $name);
                    $data_image = [
                        'file' => $name,
                    ];
                    $brand_image->fill($data_image);
                    $brand_image->save();
                }
                $data = [
                    'name' => $request->name
                ];
                $brand->fill($data);
                $brand->save();

                alert()->success('Se actualizó la marca "' . $brand->name . '" exitosamente')->autoclose(5000);
                return redirect()->route('brands.index');
            }
        }
    }

    public function destroy($id){
        $brand = Brand::where('id', $id)->with(['brandImage'])->first();
        if(file_exists(public_path($brand->brandImage->path . DIRECTORY_SEPARATOR . $brand->brandImage->file))){
            unlink(public_path($brand->brandImage->path . DIRECTORY_SEPARATOR . $brand->brandImage->file));
        }
        if(file_exists(public_path($brand->brandImage->path))){
            rmdir(public_path($brand->brandImage->path));
        }
        $brand_image = BrandImage::where('brand_id', $brand->id)->first();
        $brand_image->delete();
        $brand->delete();

        alert()->success('Se borró la marca "' . $brand->name . '" exitosamente')->autoclose(5000);
        return redirect()->route('brands.index');
    }
}

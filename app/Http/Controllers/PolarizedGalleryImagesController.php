<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\PolarizedGalleryImage;

class PolarizedGalleryImagesController extends Controller
{
    public function index(){
        $images = PolarizedGalleryImage::orderBy('id', 'asc')->get();
        return view('layouts.polarized.index')->with(compact('images'));
    }    

    public function store(Request $request){
        $messages = [
            'image_files.required' => 'Debe adjuntar al menos una imagen',
            'image_files.array' => 'El formato de presentación de imágenes es incorrecto. Por favor comuníquese con el administrador'
        ];

        $rules = [
            'image_files' => 'required|array'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            foreach($request->image_files as $image){
                $name = 'image-' . time() . $image->getClientOriginalName();
                $path = public_path('uploads' . DIRECTORY_SEPARATOR . 'polarized');
                $ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'polarized';
                $image->move($path, $name);
                $data_image = [
                    'path' => $ref_path,
                    'file' => $name,
                ];
                $image = new PolarizedGalleryImage($data_image);
                $image->save();
            }
            
            alert()->success('Se subieron las imágenes exitosamente', 'Perfecto!')->autoclose(5000);
            return redirect()->route('polarized.index');
        }
    }

    public function destroy($id){
        $image = PolarizedGalleryImage::find($id);
        if(file_exists(public_path($image->path . DIRECTORY_SEPARATOR . $image->file))){
            unlink(public_path($image->path . DIRECTORY_SEPARATOR . $image->file));
        }
        $image->delete();
        alert()->success('Se eliminó la imagen exitosamente', 'Perfecto!')->autoclose(5000);
        return redirect()->route('polarized.index');
    }
}

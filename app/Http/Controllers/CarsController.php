<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SweetAlert;
use App\User;
use App\Car;
use App\CarImage;
use App\Brand;

class CarsController extends Controller
{
    public function index(Request $request){
        $brands = Brand::orderBy('id', 'asc')->get();
        $cars = Car::CarFilter($request->get('brand_id'), $request->get('range_cost'), $request->get('range_mileage'))->with(['carImages', 'user'])->get();
        return view('layouts.cars.index')->with(compact('cars', 'brands'));
    }

    public function create(){
        $brands = Brand::orderBy('id', 'asc')->get();
        $users = User::orderBy('id', 'asc')->get();
        return view('layouts.cars.create')->with(compact('brands', 'users'));
    }

    public function store(Request $request){
        $messages = [
            'brand_id.required' => 'Debe seleccionar una marca',
            'brand_id.numeric' => 'El id de la marca debe ser un valor numérico',
            'brand_id.exists' => 'El id de la marca no está registrado en la base de datos',
            'version.required' => 'Debe especificar la versión del vehículo',
            'fuel.required' => 'Debe especificar el tipo de combustible del vehículo',
            'mileage.requried' => 'Debe especificar el kilometraje del vehículo',
            'mileage.numeric' => 'El kilometraje debe ser un valor numérico',
            'direction.required' => 'Debe especificar la dirección del vehículo',
            'transmission.required' => 'Debe especificar el tipo de transmisión del vehículo',
            'cost.required' => 'Debe especificar el valor del vehículo',
            'cost.numeric' => 'El costo del vehículo debe ser un valor numérico',
            'image_files.required' => 'Las imágenes del vehículo son obligatorias',
            'image_files.array' => 'El formato de presentación de imágenes es inválido',
            'main.required' => 'Debe adjuntar una imagen principal',
            'main.mimes' => 'La imágen principal debe ser jpg o png',
            'user_id.required' => 'Debe seleccionar un usuario a cargo',
            'user_id.numeric' => 'El id del usuario debe ser un valor numérico',
            'user_id.exists' => 'El id del usuario no está registrado en la base de datos',
        ];

        $rules = [
            'brand_id' => 'required|numeric|exists:brands,id',
            'version' => 'required',
            'fuel' => 'required',
            'mileage' => 'required|numeric',
            'direction' => 'required',
            'transmission' => 'required',
            'cost' => 'required|numeric',
            'image_files' => 'required|array',
            'main' => 'required',//|mimes:jpg,png,JPEG'
            'user_id' => 'required|numeric|exists:users,id',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            ($request->airbag == "Y") ? $airbag = "Si" : $airbag = "No";
            ($request->alarm == "Y") ? $alarm = "Si" : $alarm = "No";
            ($request->stability_control == "Y") ? $stability_control = "Si" : $stability_control = "No";
            ($request->rear_defroster == "Y") ? $rear_defroster = "Si" : $rear_defroster = "No";
            ($request->abs_brakes == "Y") ? $abs_brakes = "Si" : $abs_brakes = "No";
            ($request->air_conditioning == "Y") ? $air_conditioning = "Si" : $air_conditioning = "No";
            ($request->electric_mirrors == "Y") ? $electric_mirrors = "Si" : $electric_mirrors = "No";
            ($request->sliding_roof == "Y") ? $sliding_roof = "Si" : $sliding_roof = "No";
            ($request->rain_sensor == "Y") ? $rain_sensor = "Si" : $rain_sensor = "No";
            ($request->electric_glasses == "Y") ? $electric_glasses = "Si" : $electric_glasses = "No";
            ($request->power_seats == "Y") ? $power_seats = "Si" : $power_seats = "No";
            ($request->central_locking_of_doors == "Y") ? $central_locking_of_doors = "Si" : $central_locking_of_doors = "No";
            ($request->air_conditioner == "Y") ? $air_conditioner = "Si" : $air_conditioner = "No";
            ($request->mp3_player == "Y") ? $mp3_player = "Si" : $mp3_player = "No";
            ($request->bluetooth == "Y") ? $bluetooth = "Si" : $bluetooth = "No";
            ($request->dvd == "Y") ? $dvd = "Si" : $dvd = "No";
            ($request->usb_entry == "Y") ? $usb_entry = "Si" : $usb_entry = "No";
            $car_data = [
                'version' => $request->version,
                'fuel' => $request->fuel,
                'mileage' => $request->mileage,
                'direction' => $request->direction,
                'transmission' => $request->transmission,
                'cost' => $request->cost,
                'brand_id' => $request->brand_id,
                'model' => $request->model,
                'cylinder_capacity' => $request->cylinder_capacity,
                'doors' => $request->doors,
                'license_plate' => $request->license_plate,
                'traction' => $request->traction,
                'body_type' => $request->body_type,
                'airbag' => $airbag,
                'alarm' => $alarm,
                'stability_control' => $stability_control,
                'rear_defroster' => $rear_defroster,
                'abs_brakes' => $abs_brakes,
                'air_conditioning' => $air_conditioning,
                'electric_mirrors' => $electric_mirrors,
                'sliding_roof' => $sliding_roof,
                'rain_sensor' => $rain_sensor,
                'electric_glasses' => $electric_glasses,
                'power_seats' => $power_seats,
                'central_locking_of_doors' => $central_locking_of_doors,
                'air_conditioner' => $air_conditioner,
                'mp3_player' => $mp3_player,
                'bluetooth' => $bluetooth,
                'dvd' => $dvd,
                'usb_entry' => $usb_entry,
                'vehicle_ubication' => $request->vehicle_ubication,
                'description' => $request->description,
                'youtube_video' => $request->youtube_video,
                'user_id' => $request->user_id,
            ];
            $car = new Car($car_data);
            $car->save();

            if(!mkdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id), 0755, true)){
                $car->delete();
                alert()->error('No fue posible crear el directorio')->autoclose(5000);
                return back(); 
            } else {
                $main_file = $request->main;
                $main_image_name = 'main-' . time() . $main_file->getClientOriginalName();
                $main_file_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id);
                $main_file_ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id;
                $main_file->move($main_file_path, $main_image_name);
                $data_image = [
                    'path' => $main_file_ref_path,
                    'file' => $main_image_name,
                    'car_id' => $car->id,
                    'main' => 'Yes'
                ];
                $main_image = new CarImage($data_image);
                $main_image->save();
                foreach($request->image_files as $attachment){
                    $name = 'attachment-' . time() . $attachment->getClientOriginalName();
                    $path = public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id);
                    $ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id;
                    $attachment->move($path, $name);
                    $data_image = [
                        'path' => $ref_path,
                        'file' => $name,
                        'car_id' => $car->id,
                        'main' => 'No'
                    ];
                    $image = new CarImage($data_image);
                    $image->save();
                }
                alert()->success('Se creó el carro "' . $car->version . '" exitosamente', 'Perfecto!')->autoclose();
                return redirect()->route('cars.index');
            }
        }
    }

    public function edit($id){
        $car = Car::where('id', $id)->with(['carImages'])->first();
        $brands = Brand::orderBy('id', 'asc')->get();
        $users = User::orderBy('id', 'asc')->get();
        return view('layouts.cars.edit')->with(compact('car', 'brands', 'users'));
    }

    public function dropImage($id){
        $car_image = CarImage::find($id);
        if(file_exists(public_path($car_image->path . DIRECTORY_SEPARATOR . $car_image->file))){
            unlink(public_path($car_image->path . DIRECTORY_SEPARATOR . $car_image->file));
        }
        $car_image->delete();

        alert()->success('Imagen eliminada exitosamente', 'Perfecto!')->autoclose(5000);
        return back();
    }

    public function update(Request $request){
        $messages = [
            'car_id.required' => 'El id del vehículo es obligatorio',
            'car_id.numeric' => 'El id del vehículo debe ser un valor numérico',
            'car_id.exists' => 'El id del vehículo no está registrado en la base de datos',
            'version.required' => 'Debe especificar la versión del vehículo',
            'fuel.required' => 'Debe especificar el tipo de combustible del vehículo',
            'mileage.requried' => 'Debe especificar el kilometraje del vehículo',
            'mileage.numeric' => 'El kilometraje debe ser un valor numérico',
            'direction.required' => 'Debe especificar la dirección del vehículo',
            'transmission.required' => 'Debe especificar el tipo de transmisión del vehículo',
            'cost.required' => 'Debe especificar el valor del vehículo',
            'cost.numeric' => 'El costo del vehículo debe ser un valor numérico',
            'image_files.array' => 'El formato de presentación de imágenes es inválido',
            'main.mimes' => 'La imagen principal debe ser png o jpg',
            'user_id.required' => 'Debe seleccionar un usuario a cargo',
            'user_id.numeric' => 'El id del usuario debe ser un valor numérico',
            'user_id.exists' => 'El id del usuario no está registrado en la base de datos',
        ];

        $rules = [
            'car_id' => 'required|numeric|exists:cars,id',
            'version' => 'required',
            'fuel' => 'required',
            'mileage' => 'required|numeric',
            'direction' => 'required',
            'transmission' => 'required',
            'cost' => 'required|numeric',
            'image_files' => 'nullable|array',
            'user_id' => 'required|numeric|exists:users,id',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            alert()->error($errors, 'Ups!')->autoclose(5000);
            return back();
        } else {
            $car = Car::find($request->car_id);
            ($request->airbag == "Y") ? $airbag = "Si" : $airbag = "No";
            ($request->alarm == "Y") ? $alarm = "Si" : $alarm = "No";
            ($request->stability_control == "Y") ? $stability_control = "Si" : $stability_control = "No";
            ($request->rear_defroster == "Y") ? $rear_defroster = "Si" : $rear_defroster = "No";
            ($request->abs_brakes == "Y") ? $abs_brakes = "Si" : $abs_brakes = "No";
            ($request->air_conditioning == "Y") ? $air_conditioning = "Si" : $air_conditioning = "No";
            ($request->electric_mirrors == "Y") ? $electric_mirrors = "Si" : $electric_mirrors = "No";
            ($request->sliding_roof == "Y") ? $sliding_roof = "Si" : $sliding_roof = "No";
            ($request->rain_sensor == "Y") ? $rain_sensor = "Si" : $rain_sensor = "No";
            ($request->electric_glasses == "Y") ? $electric_glasses = "Si" : $electric_glasses = "No";
            ($request->power_seats == "Y") ? $power_seats = "Si" : $power_seats = "No";
            ($request->central_locking_of_doors == "Y") ? $central_locking_of_doors = "Si" : $central_locking_of_doors = "No";
            ($request->air_conditioner == "Y") ? $air_conditioner = "Si" : $air_conditioner = "No";
            ($request->mp3_player == "Y") ? $mp3_player = "Si" : $mp3_player = "No";
            ($request->bluetooth == "Y") ? $bluetooth = "Si" : $bluetooth = "No";
            ($request->dvd == "Y") ? $dvd = "Si" : $dvd = "No";
            ($request->usb_entry == "Y") ? $usb_entry = "Si" : $usb_entry = "No";
            $car_data = [
                'version' => $request->version,
                'fuel' => $request->fuel,
                'mileage' => $request->mileage,
                'direction' => $request->direction,
                'transmission' => $request->transmission,
                'cost' => $request->cost,
                'model' => $request->model,
                'cylinder_capacity' => $request->cylinder_capacity,
                'doors' => $request->doors,
                'license_plate' => $request->license_plate,
                'traction' => $request->traction,
                'body_type' => $request->body_type,
                'airbag' => $airbag,
                'alarm' => $alarm,
                'stability_control' => $stability_control,
                'rear_defroster' => $rear_defroster,
                'abs_brakes' => $abs_brakes,
                'air_conditioning' => $air_conditioning,
                'electric_mirrors' => $electric_mirrors,
                'sliding_roof' => $sliding_roof,
                'rain_sensor' => $rain_sensor,
                'electric_glasses' => $electric_glasses,
                'power_seats' => $power_seats,
                'central_locking_of_doors' => $central_locking_of_doors,
                'air_conditioner' => $air_conditioner,
                'mp3_player' => $mp3_player,
                'bluetooth' => $bluetooth,
                'dvd' => $dvd,
                'usb_entry' => $usb_entry,
                'vehicle_ubication' => $request->vehicle_ubication,
                'description' => $request->description,
                'youtube_video' => $request->youtube_video,
                'user_id' => $request->user_id,
            ];
            $car->fill($car_data);
            $car->save();

            if(isset($request->main)){
                $get_main_image = CarImage::where([
                    ['car_id', '=', $car->id],
                    ['main', '=', 'Yes']
                ])->get();
                if(count($get_main_image) > 0){
                    foreach($get_main_image as $image){
                        unlink(public_path($image->path . DIRECTORY_SEPARATOR . $image->file));
                        $image->delete();
                    }
                }
                $main_file = $request->main;
                $main_image_name = 'main-' . time() . $main_file->getClientOriginalName();
                $main_file_path = public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id);
                $main_file_ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id;
                $main_file->move($main_file_path, $main_image_name);
                $data_image = [
                    'path' => $main_file_ref_path,
                    'file' => $main_image_name,
                    'car_id' => $car->id,
                    'main' => 'Yes'
                ];
                $main_image = new CarImage($data_image);
                $main_image->save();
            }
            
            if(isset($request->image_files)){
                foreach($request->image_files as $attachment){
                    $name = 'attachment-' . time() . $attachment->getClientOriginalName();
                    $path = public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id);
                    $ref_path = 'uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id;
                    $attachment->move($path, $name);
                    $data_image = [
                        'path' => $ref_path,
                        'file' => $name,
                        'car_id' => $car->id,
                        'main' => 'No'
                    ];
                    $image = new CarImage($data_image);
                    $image->save();
                }
            }

            alert()->success('Se actualizó el carro "' . $car->version . '" exitosamente', 'Perfecto!')->autoclose(5000);
            return redirect()->route('cars.index');
        }
    }

    public function destroy($id){
        $car = Car::find($id);
        $car_images = CarImage::where('car_id', $car->id)->get();
        foreach($car_images as $image){
            if(file_exists(public_path($image->path . DIRECTORY_SEPARATOR . $image->file))){
                unlink(public_path($image->path . DIRECTORY_SEPARATOR . $image->file));
            }
            $image->delete();
        }
        if(file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id))){
            rmdir(public_path('uploads' . DIRECTORY_SEPARATOR . 'cars' . DIRECTORY_SEPARATOR . $car->id));
        }
        $car->delete();
        alert()->success('Se ha eliminado el carro "' . $car->version . '" exitosamente', 'Perfecto!')->autoclose(5000);
        return redirect()->route('cars.index');
    }

    public function getImages($id){
        $car_images = CarImage::where('car_id', $id)->get();
        return response()->json($car_images);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = [
        'name',
    ];

    public function brandImage(){
        return $this->hasOne('App\BrandImage', 'brand_id', 'id');
    }
}

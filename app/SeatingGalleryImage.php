<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatingGalleryImage extends Model
{
    protected $table = 'seating_gallery_images';

    protected $fillable = [
        'path',
        'file'
    ];
}

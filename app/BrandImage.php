<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandImage extends Model
{
    protected $table = 'brand_images';

    protected $fillable = [
        'path',
        'file',
        'brand_id'
    ];

    public function brand(){
        return $this->belongsTo('App\Brand', 'brand_id');
    }
}

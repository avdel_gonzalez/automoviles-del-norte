<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'FrontController@index',
    'as' => 'front.index'
]);
Route::get('/view_cars', [
    'uses' => 'FrontController@cars',
    'as' => 'front.cars'
]);
Route::get('/view_cars/carDetails/{id}', [
    'uses' => 'FrontController@carDetails',
    'as' => 'front.carDetails'
]);
Route::get('/polarized_gallery', [
    'uses' => 'FrontController@polarizedGallery',
    'as' => 'front.polarizedGallery'
]);
Route::get('/seating_gallery', [
    'uses' => 'FrontController@seatingGallery',
    'as' => 'front.seatingGallery'
]);
Route::get('/contact', [
    'uses' => 'FrontController@contactForm',
    'as' => 'front.contactForm'
]);

Route::post('/sendMail', [
    'uses' => 'FrontController@sendMail',
    'as' => 'front.sendMail'
]);

Route::post('/askByCar', [
    'uses' => 'FrontController@askByCar',
    'as' => 'front.askByCar'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    //Brands
    Route::get('/brands', [
        'uses' => 'BrandsController@index',
        'as' => 'brands.index'
    ]);
    Route::get('/brands/create', [
        'uses' => 'BrandsController@create',
        'as' => 'brands.create'
    ]);
    Route::post('/brands/store', [
        'uses' => 'BrandsController@store',
        'as' => 'brands.store'
    ]);
    Route::get('/brands/edit/{id}', [
        'uses' => 'BrandsController@edit',
        'as' => 'brands.edit'
    ]);
    Route::post('/brands/update', [
        'uses' => 'BrandsController@update',
        'as' => 'brands.update'
    ]);
    Route::get('/brands/destroy/{id}', [
        'uses' => 'BrandsController@destroy',
        'as' => 'brands.destroy'
    ]);

    //Cars
    Route::get('/cars', [
        'uses' => 'CarsController@index',
        'as' => 'cars.index'
    ]);
    Route::get('/cars/create', [
        'uses' => 'CarsController@create',
        'as' => 'cars.create'
    ]);
    Route::post('/cars/store', [
        'uses' => 'CarsController@store',
        'as' => 'cars.store'
    ]);
    Route::get('/cars/edit/{id}', [
        'uses' => 'CarsController@edit',
        'as' => 'cars.edit'
    ]);
    Route::get('/cars/dropImage/{id}', [
        'uses' => 'CarsController@dropImage',
        'as' => 'cars.dropImage'
    ]);
    Route::post('/cars/update', [
        'uses' => 'CarsController@update',
        'as' => 'cars.update'
    ]);
    Route::get('/cars/destroy/{id}', [
        'uses' => 'CarsController@destroy',
        'as' => 'cars.destroy'
    ]);
    Route::get('/cars/getImages/{car_id}', [
        'uses' => 'CarsController@getImages',
        'as' => 'cars.getImages'
    ]);

    //Polarized
    Route::get('/polarized-gallery-index', [
        'uses' => 'PolarizedGalleryImagesController@index',
        'as' => 'polarized.index'
    ]);
    Route::post('/polarized-gallery-store', [
        'uses' => 'PolarizedGalleryImagesController@store',
        'as' => 'polarized.store'
    ]);
    Route::get('/polarized-gallery-destroy/{id}', [
        'uses' => 'PolarizedGalleryImagesController@destroy',
        'as' => 'polarized.destroy'
    ]);

    //Seating
    Route::get('/seating-gallery-index', [
        'uses' => 'SeatingGalleryImagesController@index',
        'as' => 'seating.index'
    ]);
    Route::post('/seating-gallery-store', [
        'uses' => 'SeatingGalleryImagesController@store',
        'as' => 'seating.store'
    ]);
    Route::get('/seating-gallery-destroy/{id}', [
        'uses' => 'SeatingGalleryImagesController@destroy',
        'as' => 'seating.destroy'
    ]);

    //Users
    Route::get('/users', [
        'uses' => 'UsersController@index',
        'as' => 'users.index'
    ]);
    Route::get('/users/create', [
        'uses' => 'UsersController@create',
        'as' => 'users.create'
    ]);
    Route::post('/users/store', [
        'uses' => 'UsersController@store',
        'as' => 'users.store'
    ]);
    Route::get('/users/edit/{id}', [
        'uses' => 'UsersController@edit',
        'as' => 'users.edit'
    ]);
    Route::post('/users/update', [
        'uses' => 'UsersController@update',
        'as' => 'users.update'
    ]);
    Route::get('/users/destroy/{id}', [
        'uses' => 'UsersController@destroy',
        'as' => 'users.destroy'
    ]);

    //Company
    Route::get('/company', [
        'uses' => 'CompanyController@index',
        'as' => 'company.index'
    ]);
    Route::get('/company/edit/{id}', [
        'uses' => 'CompanyController@edit',
        'as' => 'company.edit'
    ]);
    Route::post('/company/update', [
        'uses' => 'CompanyController@update',
        'as' => 'company.update'
    ]);
});

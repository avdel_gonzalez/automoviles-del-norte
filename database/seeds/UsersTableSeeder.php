<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Felipe Márquez González',
            'email' => 'davidmarquez.dev@gmail.com',
            'password' => bcrypt('123456789'),
            'celphone_number' => '3217949964',
            'role' => 'admin'
        ]);

        DB::table('company_info')->insert([
            'name' => 'Automóviles del norte',
            'description' => 'Concesionaria · Asesor automotor · Tienda de repuestos de automóviles',
            'objectives' => null,
            'mission' => null,
            'vission' => null,
            'polarized_info' => null,
            'carshop_info' => null,
            'facebook' => null,
            'instagram' => null,
            'whatsapp' => null,
            'phone_number' => null,
            'footer_text' => null,
            'background_image' => null,
            'upholstery_info' => null,
            'carshop_image' => null,
            'polarized_image' => null,
            'upholstery_image' => null,
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFooterTextFieldCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function(Blueprint $table) {
            $table->longText('footer_text')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('company_info', 'footer_text')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('footer_text');
            });
        }
    }
}

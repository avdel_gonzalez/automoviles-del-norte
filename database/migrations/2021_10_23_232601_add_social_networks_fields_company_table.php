<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialNetworksFieldsCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function (Blueprint $table) {
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('phone_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('company_info', 'facebook')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('facebook');
            });
        }

        if(Schema::hasColumn('company_info', 'instagram')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('instagram');
            });
        }

        if(Schema::hasColumn('company_info', 'whatsapp')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('whatsapp');
            });
        }

        if(Schema::hasColumn('company_info', 'phone_number')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('phone_number');
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolarizedCarsFieldsCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function (Blueprint $table) {
            $table->longText('polarized_info')->nullable();
            $table->longText('carshop_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('company_info', 'polarized_info')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('polarized_info');
            });
        }

        if(Schema::hasColumn('company_info', 'carshop_info')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('carshop_info');
            });
        }
    }
}

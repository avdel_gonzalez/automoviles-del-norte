<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesFieldsToCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function (Blueprint $table) {
            $table->string('carshop_image')->nullable();
            $table->string('polarized_image')->nullable();
            $table->string('upholstery_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('company_info', 'carshop_image')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('carshop_image');
            });
        }

        if(Schema::hasColumn('company_info', 'polarized_image')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('polarized_image');
            });
        }

        if(Schema::hasColumn('company_info', 'upholstery_image')){
            Schema::table('company_info', function(Blueprint $table){
                $table->dropColumn('upholstery_image');
            });
        }
    }
}

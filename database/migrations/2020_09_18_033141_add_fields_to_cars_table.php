<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->string('model')->nullable();
            $table->string('cylinder_capacity')->nullable();
            $table->bigInteger('doors')->nullable();
            $table->string('license_plate')->nullable();
            $table->string('traction')->nullable();
            $table->string('body_type')->nullable();
            $table->string('airbag')->nullable();
            $table->enum('alarm', ['Si', 'No'])->default('No');
            $table->enum('stability_control', ['Si', 'No'])->default('No');
            $table->enum('rear_defroster', ['Si', 'No'])->default('No');
            $table->enum('abs_brakes', ['Si', 'No'])->default('No');
            $table->enum('air_conditioning', ['Si', 'No'])->default('No');
            $table->enum('electric_mirrors', ['Si', 'No'])->default('No');
            $table->enum('sliding_roof', ['Si', 'No'])->default('No');
            $table->enum('rain_sensor', ['Si', 'No'])->default('No');
            $table->enum('electric_glasses', ['Si', 'No'])->default('No');
            $table->enum('power_seats', ['Si', 'No'])->default('No');
            $table->enum('central_locking_of_doors', ['Si', 'No'])->default('No');
            $table->enum('air_conditioner', ['Si', 'No'])->default('No');
            $table->enum('mp3_player', ['Si', 'No'])->default('No');
            $table->enum('bluetooth', ['Si', 'No'])->default('No');
            $table->enum('dvd', ['Si', 'No'])->default('No');
            $table->enum('usb_entry', ['Si', 'No'])->default('No');
            $table->string('vehicle_ubication')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('cars', 'model')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('model');
            });
        }
        if(Schema::hasColumn('cars', 'cylinder_capacity')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('cylinder_capacity');
            });
        }
        if(Schema::hasColumn('cars', 'doors')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('doors');
            });
        }
        if(Schema::hasColumn('cars', 'license_plate')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('license_plate');
            });
        }
        if(Schema::hasColumn('cars', 'traction')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('traction');
            });
        }
        if(Schema::hasColumn('cars', 'body_type')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('body_type');
            });
        }
        if(Schema::hasColumn('cars', 'airbag')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('airbag');
            });
        }
        if(Schema::hasColumn('cars', 'alarm')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('alarm');
            });
        }
        if(Schema::hasColumn('cars', 'stability_control')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('stability_control');
            });
        }
        if(Schema::hasColumn('cars', 'rear_defroster')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('rear_defroster');
            });
        }
        if(Schema::hasColumn('cars', 'abs_brakes')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('abs_brakes');
            });
        }
        if(Schema::hasColumn('cars', 'air_conditioning')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('air_conditioning');
            });
        }
        if(Schema::hasColumn('cars', 'electric_mirrors')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('electric_mirrors');
            });
        }
        if(Schema::hasColumn('cars', 'sliding_roofs')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('sliding_roofs');
            });
        }
        if(Schema::hasColumn('cars', 'rain_sensor')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('rain_sensor');
            });
        }
        if(Schema::hasColumn('cars', 'electric_glasses')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('electric_glasses');
            });
        }
        if(Schema::hasColumn('cars', 'power_seats')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('power_seats');
            });
        }
        if(Schema::hasColumn('cars', 'central_locking_of_doors')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('central_locking_of_doors');
            });
        }
        if(Schema::hasColumn('cars', 'air_conditioner')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('air_conditioner');
            });
        }
        if(Schema::hasColumn('cars', 'mp3_player')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('mp3_player');
            });
        }
        if(Schema::hasColumn('cars', 'bluetooth')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('bluetooth');
            });
        }
        if(Schema::hasColumn('cars', 'dvd')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('dvd');
            });
        }
        if(Schema::hasColumn('cars', 'usb_entry')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('usb_entry');
            });
        }
        if(Schema::hasColumn('cars', 'vehicle_ubication')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('vehicle_ubication');
            });
        }
        if(Schema::hasColumn('cars', 'description')){
            Schema::table('cars', function(Blueprint $table){
                $table->dropColumn('description');
            });
        }
    }
}


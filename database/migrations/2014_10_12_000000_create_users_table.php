<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role', ['admin', 'regular']);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('brands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('brand_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path');
            $table->string('file');
            $table->bigInteger('brand_id')->unsigned();;
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });

        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('version');
            $table->string('fuel');
            $table->string('mileage');
            $table->string('direction');
            $table->string('transmission');
            $table->bigInteger('cost');
            $table->bigInteger('brand_id')->unsigned();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });

        Schema::create('car_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path');
            $table->string('file');
            $table->enum('main', ['Yes', 'No'])->default('No');
            $table->bigInteger('car_id')->unsigned();
            $table->timestamps();

            $table->foreign('car_id')->references('id')->on('cars');
        });

        Schema::create('company_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('objectives')->nullable();
            $table->text('mission')->nullable();
            $table->text('vission')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_images');
        Schema::dropIfExists('cars');
        Schema::dropIfExists('brand_images');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('company_info');
        Schema::dropIfExists('users');
    }
}
